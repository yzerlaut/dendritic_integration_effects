import numpy as np
import matplotlib
import matplotlib.pylab as plt
import fourier_for_real as rfft

### Implementing those functions

from theory import *


### let's compute the stationary voltage response

params = {
'g_pas': 1e-3*1e4, 'cm' : 1*1e-2, 'Ra' : 35.4*1e-2, 'El': -65e-3,\
'Qe' : 2e-9 , 'Te' : 5.e-3, 'Ee': 0e-3,\
'Qi' : 1e-9 , 'Ti' : 5.e-3, 'Ei': -80e-3,\
'seed' : 0}

# ball (soma)
soma = {'L': 10*1e-6, 'D': 10*1e-6, 'NSEG': 1}
# stick
stick = {'L': 2000*1e-6, 'D': 4*1e-6, 'NSEG': 100}
params_for_cable_theory(stick, params)


# mean conductance
Garray = [0,0,0]

dt, tstop = 1e-5, 40e-3
t = np.arange(int(tstop/dt))*dt
Q, t0, Tsyn = params['Qe'], 5e-3, params['Te']

# now let's compute the Fourier quantities, see modules implementation
f = rfft.time_to_freq(len(t), dt)
FTevent_th = exp_FT(f, Q, Tsyn, t0=t0)

L = stick['L']
x = np.linspace(0,L,1e3) # x vector
muV = stat_pot_function(x, Garray, soma, stick, params)
V = np.zeros((len(x),len(t))) # membrane potential response in space and time

X0 = L/3. # synaptic input location

for ix in range(len(x)):
    # TF = np.array([\
    #     dv_kernel_per_I(x[ix], X0, ff, Garray, stick, soma, params)\
    #     for ff in f])
    TF = dv_kernel_per_I(x[ix], X0, f, Garray, stick, soma, params)
    TF *= (params['Ee']-muV[int(X0/x[1])])
    V[ix,:] = muV[ix]+rfft.inv_FT(TF*FTevent_th, len(t), dt)
    
mymap = matplotlib.colors.LinearSegmentedColormap.from_list(\
    'mycolors',['blue','red'])
Z = [[0,0],[0,0]]
t_plot = np.array([5,5.2,6,7,8,10,20,25])#*int(1./dt/1e3)
c = np.arange(len(t_plot))*1./len(t_plot)
CS3 = plt.contourf(Z, c, cmap=mymap)
plt.clf()

fig = plt.figure(figsize=(14,4))
plt.subplot(121)
plt.title('analytic approx')

for i in range(len(c)):
    r = c[i]
    plt.plot(1e6*x, 1e3*V[:,int(t_plot[i]/dt/1e3)], color=mymap(r,1))

cb = plt.colorbar(CS3,use_gridspec=True) ## TO BE ADDED
cmin,cmax = cb.get_clim()
ticks = np.linspace(cmin,cmax,len(t_plot))
cb.set_ticks(ticks)
cb.set_ticklabels(['%.5g' % tt for tt in t_plot])

cb.set_label('time (ms)')
plt.xlabel('position ($\mu$m)')
plt.ylabel('$v$ (mV)')
plt.ylim([-65.1,-64.2])


#### Now Neuron simulation
plt.subplot(122)
plt.title('NEURON sim')

from neuron import h as nrn
nrn('create soma, dend')
nrn('connect soma(1), dend(0)')
nrn("objref nil, nc, syn")

print nrn.topology()
nrn.soma.nseg, nrn.soma.L, nrn.soma.diam = 1,\
   1e6*soma['L'], 1e6*soma['D']#79.8, 79.8
nrn.soma.Ra = params['Ra']*1e2
nrn.soma.cm = params['cm']*1e2
nrn.soma.insert('pas')
nrn.soma.g_pas = params['g_pas']*1e-4
nrn.soma.e_pas = params['El']*1e3
nrn.dend.nseg, nrn.dend.L, nrn.dend.diam = len(x),\
   1e6*stick['L'], 1e6*stick['D']
nrn.dend.Ra = params['Ra']*1e2
nrn.dend.cm = params['cm']*1e2
nrn.dend.insert('pas')
nrn.dend.g_pas = params['g_pas']*1e-4
nrn.dend.e_pas = params['El']*1e3

syn = nrn.ExpSyn(X0/stick['L'], sec = nrn.dend)
nc = nrn.NetCon(nrn.nil, syn)
syn.tau, syn.e = 1e3*Tsyn, 1e3*params['Ee']
nc.weight[0] = Q*1e6
nrn.dt = 1e3*dt
nrn.finitialize()
nc.event(1e3*t0)
V = np.zeros((len(x), len(t)))
i = 0
ix = np.linspace(0,1, len(x))
while nrn.t<1e3*t[-1]:
    nrn.fadvance()
    for j in range(len(x)):
        V[j,i] = nrn.dend(ix[j])._ref_v[0]
    i+=1

for i in range(len(c)):
    r = c[i]
    plt.plot(1e6*x, V[:,int(t_plot[i]/dt/1e3)], color=mymap(r,1))

cb = plt.colorbar(CS3,use_gridspec=True) ## TO BE ADDED
cmin,cmax = cb.get_clim()
ticks = np.linspace(cmin,cmax,len(t_plot))
cb.set_ticks(ticks)
cb.set_ticklabels(['%.3g' % tt for tt in t_plot])

cb.set_label('time (ms)')
plt.xlabel('position ($\mu$m)')
plt.ylabel('$v$ (mV)')
plt.ylim([-65.1,-64.2])

plt.tight_layout()
plt.show()



print '---------------------------------------------------------------------------'
print '---------------------------------------------------------------------------'
print ' NOW CHECKING THE VALUES OF THE INTEGRALS'
print '---------------------------------------------------------------------------'


print '=> at x=0'
# now let's compute the Fourier quantities, see modules implementation
# t = np.arange(int(tstop/dt)-3)*dt
# f = rfft.time_to_freq(len(t), dt)
# FTevent_th = Q*np.exp(-1j*2*np.pi*t0*f)/(1j*2*np.pi*f+1./Tsyn)
# TF = 1e3*dv_kernel(0, X0, f, Garray, Params)
v1 = 1e-3*np.array(\
    [V[0,i]-params['El']*1e3 if V[0,i]<-30 else 0 for i in range(len(t))])
TF = dv_kernel_per_I(0., X0, f, Garray, stick, soma, params)
TF *= (params['Ee']-muV[int(X0/x[1])])
v2 = rfft.inv_FT(TF*FTevent_th, len(t), dt)


print 'temporal integral :', np.trapz(v1*v1, t)
print 'fourier integral :', 2*np.trapz(np.abs(TF*FTevent_th)**2, f)
plt.plot(1e3*t, 1e3*v1, 'r', label='num. sim.')
plt.plot(1e3*t, 1e3*v2, 'b', label='analytic approx.')
plt.xlabel('time (ms)')
plt.ylabel('$\Delta V_m $ (mV)')
plt.legend()
plt.show()



