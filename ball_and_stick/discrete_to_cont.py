import numpy as np
import matplotlib
import matplotlib.pylab as plt
import fourier_for_real as rfft

### Implementing those functions

from theory import *


### let's compute the stationary voltage response

params = {
'g_pas': 1e-3*1e4, 'cm' : 1*1e-2, 'Ra' : 35.4*1e-2, 'El': -65e-3,\
'Qe' : .2e-9 , 'Te' : 10.e-3, 'Ee': 0e-3,\
'Qi' : .8e-9 , 'Ti' : 10.e-3, 'Ei': -80e-3,\
'seed' : 0}

# ball (soma)
soma = {'L': 20*1e-6, 'D': 20*1e-6, 'NSEG': 1,
        'exc_density':1e9, 'inh_density':25*1e-12,
        'Ke_per_seg':1., 'Ki_per_seg':1., 'name':'soma'}
# stick
stick = {'L': 1000*1e-6, 'D': 2*1e-6, 'NSEG': 30,\
         'exc_density':17*1e-12, 'inh_density':100*1e-12,
         'Ke_per_seg':24, 'Ki_per_seg':4., 'name':'dend'}
cables = [soma, stick]

# density of synapses in um2, one synapses per this area !
# so calculate synapse number
Ke_tot, Ki_tot = 0, 0
for i in range(len(cables)):
    cables[i]['Ki_per_seg'] = cables[i]['L']*\
          cables[i]['D']*np.pi/cables[i]['NSEG']/cables[i]['inh_density']
    Ki_tot += cables[i]['Ki_per_seg']*cables[i]['NSEG']
    cables[i]['Ke_per_seg'] = cables[i]['L']*\
          cables[i]['D']*np.pi/cables[i]['NSEG']/cables[i]['exc_density']
    Ke_tot += cables[i]['Ke_per_seg']*cables[i]['NSEG']
print "Total number of EXCITATORY synapses : ", Ke_tot
print "Total number of INHIBITORY synapses : ", Ki_tot


dx = stick['L']/stick['NSEG']
x = np.concatenate([[0],dx/2.+np.arange(stick['NSEG'])*dx])
params_for_cable_theory(stick, params)

fe, fi = 3.,5.
Garray = calculate_mean_conductances(fe, fi, soma, stick, params)
muV = stat_pot_function(x, Garray, soma, stick, params)
print 'Conductances (ratio)'
print Garray[0]*stick['rm'], Garray[1]*stick['rm'],\
  Garray[2]/(params['g_pas']*np.pi*soma['L']*soma['D'])

from nrn_simulations import *
exc_synapses, exc_netcons, exc_netstims,\
       inh_synapses, inh_netcons, inh_netstims,\
       area_lists, spkout =\
          Constructing_the_ball_and_tree(params, cables,
                                spiking_mech=False)

for i in range(len(cables)):
    for j in range(len(area_lists[i])):
        # excitation
        Ke = cables[i]['Ke_per_seg']
        exc_netstims[i][j].interval = 1e3/fe/Ke
        Ki = cables[i]['Ki_per_seg']
        inh_netstims[i][j].interval = 1e3/fi/Ki

        
dt, tstop = 0.01, 1000.
        
## --- recording
t_vec = nrn.Vector()
t_vec.record(nrn._ref_t)
## --- launching the simulation
nrn.finitialize(params['El']*1e3)
nrn.dt = dt
V = np.zeros((int(tstop/dt),len(x)))
i=0
while nrn.t<(tstop-dt):
    V[i,0] = nrn.cable_0_0(.5)._ref_v[0]
    j = 1
    for seg in nrn.cable_1_0:
        V[i,j] = nrn.cable_1_0(seg.x)._ref_v[0]
        j+=1
    i+=1
    nrn.fadvance()
print '-------------------------------------------'
print '---------END SIMULATION -------------------'
print '-------------------------------------------'

V = np.array(V)
t = np.array(t_vec)[:-1]

plt.errorbar(1e6*x, V[1000:,:].mean(axis=0), V[1000:,:].std(axis=0), color='b')
plt.xlim([1e6*x[0]-10,1e6*stick['L']])

dt, tstop = 1e-4, 40e-3
t = np.arange(int(tstop/dt))*dt
# now let's compute the Fourier quantities, see modules implementation
f = rfft.time_to_freq(len(t), dt)

L = stick['L']

print '======================================'
print '-------------------------------------------'
print '---------DISCRETE SUMMATION -------------------'
print '-------------------------------------------'

sv2 = np.zeros(len(x))
for ix_dest in range(len(x)):
    for ix_source in range(1, len(x)):
        TF0 = dv_kernel_per_I(x[ix_dest], x[ix_source],\
                    f, Garray, stick, soma, params)
                    
        # excitatory synapse
        TF = TF0*(params['Ee']-muV[ix_source])
        TF *= exp_FT(f, params['Qe'], params['Te'])
        sv2[ix_dest] += fe*stick['Ke_per_seg']*\
          2*np.trapz(np.abs(TF)**2, f)
          
        # inhibitory synapse at dendrites
        TF = TF0*(params['Ei']-muV[ix_source])
        TF *= exp_FT(f, params['Qi'], params['Ti'])
        sv2[ix_dest] += fi*stick['Ki_per_seg']*\
          2*np.trapz(np.abs(TF)**2, f)

    # inhibitory synapse at soma
    TF0 = dv_kernel_per_I(x[ix_dest], 0.,\
                    f, Garray, stick, soma, params)
    TF = TF0*(params['Ei']-muV[0])
    TF *= exp_FT(f, params['Qi'], params['Ti'])
    sv2[ix_dest] += fi*soma['Ki_per_seg']*\
          2*np.trapz(np.abs(TF)**2, f)
std = np.sqrt(sv2)

plt.plot(1e6*x, 1e3*(muV), 'r', lw=4)
plt.plot(1e6*x, 1e3*(std+muV), 'r', lw=2)
plt.plot(1e6*x, 1e3*(muV-std), 'r', lw=2)


print '======================================'
print '-------------------------------------------'
print '---------CONTINUOUS APPROX -------------------'
print '-------------------------------------------'


muV = stat_pot_function(x, Garray, soma, stick, params)
dx =x[1]-x[0]

sv2 = np.zeros(len(x))
for ix_dest in range(len(x)):
    X1 = np.linspace(0, x[ix_dest], 1e3)
    muV1 = stat_pot_function(X1, Garray, soma, stick, params)
    X2 = np.linspace(x[ix_dest], L, 1e3)
    muV2 = stat_pot_function(X2, Garray, soma, stick, params)
    output, TF1, TF2 = np.zeros(len(f)), np.zeros(len(f)), np.zeros(len(f))
    
    for i in range(len(f)):
        # excitatory synapse
        TF1[i] = np.trapz(\
                    dv_kernel_per_I_X_x2(x[ix_dest], X1,\
                    f[i], Garray, params['Ee'], muV1,\
                    stick, soma, params), X1)+\
                    np.trapz(\
                    dv_kernel_per_I_x_X2(x[ix_dest], X2,\
                    f[i], Garray, params['Ee'], muV2,\
                    stick, soma, params), X2)
        # # inhibitory synapse
        TF2[i] = np.trapz(\
                    dv_kernel_per_I_X_x2(x[ix_dest], X1,\
                    f[i], Garray, params['Ei'], muV1,\
                    stick, soma, params), X1)+\
                    np.trapz(\
                    dv_kernel_per_I_x_X2(x[ix_dest], X2,\
                    f[i], Garray, params['Ei'], muV2,\
                    stick, soma, params), X2)

                    
    # inhibitory synapse at soma
    TF = np.abs(dv_kernel_per_I(x[ix_dest], 0.,\
                    f, Garray, stick, soma, params))**2*\
                    (params['Ei']-muV[0])**2*\
                    exp_FT_mod(f, params['Qi'], params['Ti'])
                    
    sv2[ix_dest] = 2*(\
        fi*soma['Ki_per_seg']*np.trapz(TF, f)+\
        fe*stick['D']*np.pi/stick['exc_density']*np.trapz(TF1*\
                    exp_FT_mod(f, params['Qe'], params['Te']), f)+\
        fi*stick['D']*np.pi/stick['inh_density']*np.trapz(TF2*\
                    exp_FT_mod(f, params['Qi'], params['Ti']), f))
        
std_th = np.sqrt(sv2)
plt.plot(1e6*x, 1e3*(std_th+muV), 'k--', lw=2)
plt.plot(1e6*x, 1e3*(muV-std_th), 'k--', lw=2)


sv2 = np.zeros(len(x))
for ix_dest in range(len(x)):

    TF = np.zeros(len(f))
    # excitatory synapse at dendrites
    Gf2 = exp_FT_mod(f, params['Qe'], params['Te'])
    TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
                f, Gf2, fe, params['Ee'], 1./stick['exc_density'],\
                Garray, soma, stick, params)
                
    # inhibitory synapse at dendrites
    Gf2 = exp_FT_mod(f, params['Qi'], params['Ti'])
    TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
                f, Gf2, fi, params['Ei'], 1./stick['inh_density'],\
                Garray, soma, stick, params)

    # inhibitory synapse at soma
    TF0 = dv_kernel_per_I(x[ix_dest], 0.,\
                    f, Garray, stick, soma, params)
    TF2 = TF0*(params['Ei']-muV[0])
    TF2 *= exp_FT(f, params['Qi'], params['Ti'])
    TF += 2*fi*soma['Ki_per_seg']*np.abs(TF2)**2


    # ##then Fourier transform !
    sv2[ix_dest] = np.trapz(TF, f)
std_th = np.sqrt(sv2)


print '----------------------------------------------------'
print ' end calculus'
print '----------------------------------------------------'


plt.fill_between(1e6*x, 1e3*(std_th+muV), 1e3*(muV-std_th),\
                 color='g', alpha=.3)

plt.show()


