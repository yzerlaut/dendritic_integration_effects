import numpy as np
import matplotlib
import matplotlib.pylab as plt
import fourier_for_real as rfft


def params_for_cable_theory(cable, Params):
    """
    applies the radial symmetry, to get the linear cable theory parameters
    """
    D = cable['D']
    cable['rm'] = 1./Params['g_pas']/(np.pi*D) # [O.m]   """" NEURON 1e-1 !!!! """" 
    cable['ri'] = Params['Ra']/(np.pi*D**2/4) # [O/m]
    cable['cm'] = Params['cm']*np.pi*D # [F/m] """" NEURON 1e-2 !!!! """"


def calculate_mean_conductances(fe, fi, soma, cable, Params):
    """
    this calculates the conductances that needs to plugged in into the
    linear cable equation
    Note :
    the two first conductances are RADIAL conductances !! (excitation and
    inhibition along the cable)
    the third one is an ABSOLUTE conductance !
    """
    L, D = cable['L'], cable['D']
    area = L*D*np.pi

    # radial density of excitatory conductance
    Te, Qe = Params['Te'], Params['Qe']
    Ke_tot = area/cable['exc_density']
    # print 'excitatory synapses on cable : ', Ke_tot
    ge = Qe*Ke_tot*fe*Te/L
    
    # radial density of inhibitory conductance
    Ti, Qi = Params['Ti'], Params['Qi']
    Ki_tot = area/cable['inh_density']
    # print 'inhibitory synapses on cable : ', Ki_tot
    gi = Qi*Ki_tot*fi*Ti/L

    # radial density of inhibitory conductance
    Ls, Ds = soma['L'], soma['D']
    Kis = np.pi*Ls*Ds/soma['inh_density']
    Gi = Qi*Kis*fi*Ti
    # print 'Gi soma (nS) : ', 1e9*Gi
    
    return ge, gi, Gi

    
def parameters_for_mean(ge0, gi0, Gi0, soma, stick, Params):
    params_for_cable_theory(stick, Params)
    
    [Ls, Ds] = soma['L'], soma['D']
    [L, D] = stick['L'], stick['D']
    Gl = np.pi*Ls*Ds*Params['g_pas']
    Cm = np.pi*Ls*Ds*Params['cm']
    [El, Ei, Ee] = Params['El'], Params['Ei'], Params['Ee']
    [rm, cm, ri] = stick['rm'], stick['cm'], stick['ri']
    tau = rm*cm/(1+rm*ge0+rm*gi0)
    Tau = Cm/(Gl+Gi0)
    lbd = np.sqrt(rm/ri/(1+rm*ge0+rm*gi0))
    v0 = (El+rm*ge0*Ee+rm*gi0*Ei)/(1+rm*ge0+rm*gi0)
    V0 = (Gl*El+Gi0*Ei)/(Gl+Gi0)
    return np.array([tau, Tau, lbd, v0, V0])


def stat_pot_function(x, Garray, soma, stick, Params):
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                        soma, stick, Params)
    [ri, L] = stick['ri'], stick['L']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    denom = 1/np.tanh(L/lbd)+(Tau)/(ri*Cm*lbd)
    return v0+(V0-v0)/denom*np.cosh((x-L)/lbd)/np.sinh(L/lbd)

def parameters_for_mean_BRT(fe, fi, cables, Params):

    soma = cables[0]
    [Ls, Ds] = soma['L'], soma['D']
    Gl = np.pi*Ls*Ds*Params['g_pas']
    print 'soma conductac', Gl*1e9
    Cm = np.pi*Ls*Ds*Params['cm']
    [El, Ei, Ee] = Params['El'], Params['Ei'], Params['Ee']
    
    # radial density of excitatory conductance
    Te, Qe = Params['Te'], Params['Qe']
    ge0_rm = Qe*fe*Te/Params['exc_density_dend']/Params['g_pas']
    
    # radial density of inhibitory conductance
    Ti, Qi = Params['Ti'], Params['Qi']
    gi0_rm = Qi*fi*Ti/Params['inh_density_dend']/Params['g_pas']
    
    # radial density of inhibitory conductance at soma
    Gi0_Rm = Qi*fi*Ti/Params['inh_density_soma']/Params['g_pas']
    
    print 'Conductances ratio :', Gi0_Rm, gi0_rm, ge0_rm

    
    # potentials
    v0 = (El+ge0_rm*Ee+gi0_rm*Ei)/(1+ge0_rm+gi0_rm)
    V0 = (El+Gi0_Rm*Ei)/(1+Gi0_Rm)

    print ' potentials :', 1e3*v0, 1e3*V0
    # time constants (doesn't depends on the diameter)
    Tau = Params['cm']/Params['g_pas']/(1+Gi0_Rm)
    tau = Params['cm']/Params['g_pas']/(1+ge0_rm+gi0_rm)

    # now the different lambda, as a power of the first lbd=lbd0
    LBD, LL, Lmax = [], [], 0
    D = cables[1]['D']
    lbd0 = np.sqrt(D/4/Params['g_pas']/Params['Ra']/(1+ge0_rm+gi0_rm))
    for i in range(1, len(cables)): # discard soma
        lbd = lbd0*2**(-(i-1)/3.)
        LBD.append(lbd)
        LL.append(cables[i]['L'])
        Lmax += cables[i]['L']/lbd
    return [tau, Tau, np.array(LBD), np.array(LL), Lmax, v0, V0]

def rescale_x_BRT(x, LL, LBD):
    Lsum = np.cumsum(np.concatenate([[0],LL]))
    i, X = 1, 0
    while (x>Lsum[i]) :
        X+=LL[i-1]/LBD[i-1]
        i+=1
    return X+(x-Lsum[i-1])/LBD[i-1]

    
def stat_pot_function_BRT(x, fe, fi, cables, Params):
    
    soma = cables[0]
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    
    tau, Tau, LBD, LL, Lmax, v0, V0 = \
      parameters_for_mean_BRT(fe, fi, cables, Params)

    ri = Params['Ra']/(np.pi*cables[1]['D']**2/4) # [O/m]
    denom = 1/np.tanh(Lmax)+(Tau)/(ri*Cm*LBD[0])
    X = np.array([rescale_x_BRT(xx, LL, LBD) for xx in x])
    print X.max(), Lmax
    return v0+(V0-v0)/denom*np.cosh(X-Lmax)/np.sinh(Lmax)


def dv_kernel_per_I(x, X, f, Garray, stick, soma, Params):
    [ri, L, D, cm] = stick['ri'], stick['L'], stick['D'], stick['cm']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                                    soma, stick, Params)
    Af = 1./(1+2*1j*np.pi*f*tau) # unitary input for the kernel
    lbdF = lbd/np.sqrt(1+2*1j*np.pi*f*tau)
    Bf = lbdF*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau
    factor = Af*tau/cm/lbdF/( np.sinh(L/lbdF) + Bf*np.cosh(L/lbdF) )
    if x<X:
        func = (np.cosh(x/lbdF) + Bf*np.sinh(x/lbdF))*np.cosh((X-L)/lbdF)
    else:
        func = (np.cosh(X/lbdF) + Bf*np.sinh(X/lbdF))*np.cosh((x-L)/lbdF)
    return func*factor

def expr0(U, B, af, bf):      
    return np.abs( np.sinh(U*(af+1j*bf)) + B*np.cosh(U*(af+1j*bf)) )**2

def expr1(U, af, bf):
    # return np.abs(np.cosh(U*(af+1j*bf)))**2
    return .5*(np.cosh(2*af*U)+np.cos(2*bf*U))
    
def expr2(U, B, af, bf):      
    return np.abs( np.cosh(U*(af+1j*bf)) + B*np.sinh(U*(af+1j*bf)) )**2



def dv_kernel_per_I_X_x2(x, X, f, Garray, Erev, muV,
                    stick, soma, Params):
    """
    Kernel when X (the input location) is greater than the
    location of the destination
    e.g. to be used only for the soma !!
    """
    [ri, L, D, cm] = stick['ri'], stick['L'], stick['D'], stick['cm']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                                    soma, stick, Params)
    Af2 = 1./(1+(2*np.pi*f*tau)**2) # unitary input for the kernel
    af, bf = split_root_square_of_imaginary(f, tau)
    Bf = lbd*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau/(af+1j*bf)
    factor = tau**2/cm**2/lbd**2*(af**2+bf**2)/\
      expr0(L/lbd, Bf, af, bf)
    ff = expr2(X/lbd, Bf, af, bf)
    gg = expr1((x-L)/lbd, af, bf)
    return Af2*factor*ff*gg*(Erev-muV)**2


def dv_kernel_per_I_x_X2(x, X, f, Garray, Erev, muV,
                    stick, soma, Params):
    """
    Kernel when X (the input location) is smaller than the
    location of the destination
    """
    [ri, L, D, cm] = stick['ri'], stick['L'], stick['D'], stick['cm']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                                    soma, stick, Params)
    Af2 = 1./(1+(2*np.pi*f*tau)**2) # unitary input for the kernel
    af, bf = split_root_square_of_imaginary(f, tau)
    Bf = lbd*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau/(af+1j*bf)
    factor = tau**2/cm**2/lbd**2*(af**2+bf**2)/\
      expr0(L/lbd, Bf, af, bf)
    ff = expr2(x/lbd, Bf, af, bf)
    gg = expr1((X-L)/lbd, af, bf)
    return Af2*factor*ff*gg*(Erev-muV)**2


def exp_FT(f, Q, Tsyn, t0=0):
    return Q*np.exp(-1j*2*np.pi*t0*f)/(1j*2*np.pi*f+1./Tsyn)

def exp_FT_mod(f, Q, Tsyn):
    return Q**2/((2*np.pi*f)**2+(1./Tsyn)**2)


def split_root_square_of_imaginary(f, tau):
    # returns the ral and imaginary part of Sqrt(1+2*Pi*f*tau)
    af = np.sqrt((np.sqrt(1+(2*np.pi*f*tau)**2)+1)/2)
    bf = np.sqrt((np.sqrt(1+(2*np.pi*f*tau)**2)-1)/2)
    return af, bf


def dv2_contrib_per_dend_synapse_type(x, f, Gf2,\
                            syn_freq, Erev, Density, Garray,\
                            soma, stick, Params,
                            precision=1e2):
    [ri, L, D, cm] = stick['ri'], stick['L'], stick['D'], stick['cm']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                                    soma, stick, Params)
    v1 = (V0-v0)/( 1/np.tanh(L/lbd)+(Tau)/(ri*Cm*lbd) )/np.sinh(L/lbd)
    
    Af2 = 1./(1+(2*np.pi*f*tau)**2) # unitary input for the kernel
    af, bf = split_root_square_of_imaginary(f, tau)
    Bf = lbd*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau/(af+1j*bf)

    X1 = np.linspace(0, x, precision)
    X2 = np.linspace(x, L, precision)
    
    output = np.zeros(len(f))
    for i in range(len(f)):
        factor = Af2[i]*tau**2/cm**2/lbd**2*(af[i]**2+bf[i]**2)/\
          expr0(L/lbd, Bf[i], af[i], bf[i])

        Y1 = (Erev-v0-v1*np.cosh((X1-L)/lbd))**2\
                  *expr2(X1/lbd, Bf[i], af[i], bf[i])
        Y2 = (Erev-v0-v1*np.cosh((X2-L)/lbd))**2\
                  *expr1((X2-L)/lbd, af[i], bf[i])
        output[i] = factor*(\
            expr1((x-L)/lbd, af[i], bf[i])*np.trapz(Y1, X1)+\
            expr2(x/lbd, Bf[i], af[i], bf[i])*np.trapz(Y2, X2))


    return 2*syn_freq*np.pi*D*Density*output*Gf2

def Tv_contrib_per_dend_synapse_type(x, f, Gf2,\
                            syn_freq, Erev, Density, Garray,\
                            soma, stick, Params,
                            precision=1e2):
    """
    to be written , not woring yet !
    """


def get_the_theoretical_variance(fe, fi, f, x, params, soma, stick):
    sv2 = np.zeros(len(x))
    Garray = calculate_mean_conductances(fe, fi, soma, stick, params)
    muV = stat_pot_function(x, Garray, soma, stick, params)
    for ix_dest in range(len(x)):

        TF = np.zeros(len(f))
        # excitatory synapse at dendrites
        Gf2 = exp_FT_mod(f, params['Qe'], params['Te'])
        TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
                    f, Gf2, fe, params['Ee'], 1./stick['exc_density'],\
                    Garray, soma, stick, params)

        # inhibitory synapse at dendrites
        Gf2 = exp_FT_mod(f, params['Qi'], params['Ti'])
        TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
                    f, Gf2, fi, params['Ei'], 1./stick['inh_density'],\
                    Garray, soma, stick, params)

        # inhibitory synapse at soma
        TF0 = dv_kernel_per_I(x[ix_dest], 0.,\
                        f, Garray, stick, soma, params)
        TF2 = TF0*(params['Ei']-muV[0])
        TF2 *= exp_FT(f, params['Qi'], params['Ti'])
        TF += 2*fi*soma['Ki_per_seg']*np.abs(TF2)**2


        # ##then Fourier transform !
        sv2[ix_dest] = np.trapz(TF, f)
        
    return sv2

def get_the_input_and_transfer_resistance(fe, fi, f, x, params, soma, stick):
    Rin, Rtf = np.zeros(len(x)), np.zeros(len(x))
    Garray = calculate_mean_conductances(fe, fi, soma, stick, params)
    
    for ix_dest in range(len(x)):

        Rtf[ix_dest] = dv_kernel_per_I(0., x[ix_dest],
                0., Garray, stick, soma, params) # at 0 frequency
        Rin[ix_dest] = dv_kernel_per_I(x[ix_dest], x[ix_dest],
                0., Garray, stick, soma, params) # at 0 frequency
    return Rin, Rtf


### NOT WORKING FROM HERE !!!!!!!!!

def integrate_ch2_sinh(A, b, a, u1, u2): 
    """
    CForm[FullSimplify[Integrate[(A - np.cosh[u - b])**2*np.sinh[2*u*a], {u, u1, u2}]]]
    """
    # return ((16*np.power(a,2)*(-1 + np.power(a,2))*A*np.cosh(b - u1) -         (-1 + 4*np.power(a,2))*((-1 + np.power(a,2))*(1 + 2*np.power(A,2)) +            np.power(a,2)*np.cosh(2*(b - u1))))*np.cosh(2*a*u1) +      (-16*np.power(a,2)*(-1 + np.power(a,2))*A*np.cosh(b - u2) +         (-1 + 4*np.power(a,2))*((-1 + np.power(a,2))*(1 + 2*np.power(A,2)) +            np.power(a,2)*np.cosh(2*(b - u2))))*np.cosh(2*a*u2) +      2*a*(4*(-1 + np.power(a,2))*A + (1 - 4*np.power(a,2))*np.cosh(b - u1))*      np.sinh(b - u1)*np.sinh(2*a*u1) -      2*a*(4*(-1 + np.power(a,2))*A + (1 - 4*np.power(a,2))*np.cosh(b - u2))*      np.sinh(b - u2)*np.sinh(2*a*u2))/(4.*(a - 5*np.power(a,3) + 4*np.power(a,5)))
    u = np.linspace(u1, u2, 1e2)
    return np.trapz((A-np.cosh(u-b))*(A-np.cosh(u-b))*np.sinh(2*u*a), u)

def integrate_ch2_cosh(A, b, a, u1, u2):
    """
    CForm[FullSimplify[Integrate[(A - np.cosh[u - b])**2*np.cosh[2*u*a], {u, u1, u2}]]]
    """
    # return (2*a*(4*(-1 + np.power(a,2))*A + (1 - 4*np.power(a,2))*np.cosh(b - u1))*np.cosh(2*a*u1)*      np.sinh(b - u1) + (16*np.power(a,2)*(-1 + np.power(a,2))*A*np.cosh(b - u1) -         (-1 + 4*np.power(a,2))*((-1 + np.power(a,2))*(1 + 2*np.power(A,2)) +            np.power(a,2)*np.cosh(2*(b - u1))))*np.sinh(2*a*u1) -      2*a*(4*(-1 + np.power(a,2))*A + (1 - 4*np.power(a,2))*np.cosh(b - u2))*      np.cosh(2*a*u2)*np.sinh(b - u2) +      (-16*np.power(a,2)*(-1 + np.power(a,2))*A*np.cosh(b - u2) +         (-1 + 4*np.power(a,2))*((-1 + np.power(a,2))*(1 + 2*np.power(A,2)) +            np.power(a,2)*np.cosh(2*(b - u2))))*np.sinh(2*a*u2))/(4.*(a - 5*np.power(a,3) + 4*np.power(a,5)))
    u = np.linspace(u1, u2, 1e2)
    return np.trapz((A-np.cosh(u-b))*(A-np.cosh(u-b))*np.cosh(2*u*a), u)

def integrate_ch2_cos(A, b, a, u1, u2):
    """
    CForm[FullSimplify[Integrate[(A - np.cosh[u - b])**2*np.cos[2*u*a], {u, u1, u2}]]]
    """
    # return -(-16*np.power(a,2)*(1 + np.power(a,2))*A*np.cosh(b - u1)*np.sin(2*a*u1) +             np.power(a,2)*(1 + 4*np.power(a,2))*np.cosh(2*(b - u1))*np.sin(2*a*u1) +        (1 + np.power(a,2))*(1 + 4*np.power(a,2))*(1 + 2*np.power(A,2))*       (np.sin(2*a*u1) - np.sin(2*a*u2)) -         a*(a*(-16*(1 + np.power(a,2))*A*np.cosh(b - u2) +             (1 + 4*np.power(a,2))*np.cosh(2*(b - u2)))*np.sin(2*a*u2) +          2*np.cos(2*a*u1)*(-4*(1 + np.power(a,2))*A +             (1 + 4*np.power(a,2))*np.cosh(b - u1))*np.sinh(b - u1)) -         8*(a + np.power(a,3))*A*np.cos(2*a*u2)*np.sinh(b - u2) +         (a + 4*np.power(a,3))*np.cos(2*a*u2)*np.sinh(2*(b - u2)))/       (4.*(a + 5*np.power(a,3) + 4*np.power(a,5)))
    u = np.linspace(u1, u2, 1e2)
    return np.trapz((A-np.cosh(u-b))*(A-np.cosh(u-b))*np.cos(2*u*a), u)

def integrate_ch2_sin(A, b, a, u1, u2):
    """
    CForm[FullSimplify[Integrate[(A - np.cosh[u])**2*np.sin[2*u*a], {u, u1, u2}]]]
    """
    # return (-(np.cos(2*a*u1)*(16*np.power(a,2)*(1 + np.power(a,2))*A*np.cosh(u1) -           (1 + 4*np.power(a,2))*((1 + np.power(a,2))*(1 + 2*np.power(A,2)) +              np.power(a,2)*np.cosh(2*u1)))) +        np.cos(2*a*u2)*(16*np.power(a,2)*(1 + np.power(a,2))*A*np.cosh(u2) -         (1 + 4*np.power(a,2))*((1 + np.power(a,2))*(1 + 2*np.power(A,2)) +            np.power(a,2)*np.cosh(2*u2))) -        2*a*(-4*(1 + np.power(a,2))*A + (1 + 4*np.power(a,2))*np.cosh(u1))*np.sin(2*a*u1)*       np.sinh(u1) + 2*a*(-4*(1 + np.power(a,2))*A + (1 + 4*np.power(a,2))*np.cosh(u2))*       np.sin(2*a*u2)*np.sinh(u2))/(4.*(a + 5*np.power(a,3) + 4*np.power(a,5)))
    u = np.linspace(u1, u2, 1e2)
    return np.trapz((A-np.cosh(u-b))*(A-np.cosh(u-b))*np.sin(2*u*a), u)


def sum_expr_ch_sh_sin_cos(X, lbd, af, bf, Cf, Df, Ef):
    A = Cf*np.cosh(2*af*X/lbd)
    B = Df*np.sinh(2*af*X/lbd)
    C = Df*np.sin(2*bf*X/lbd)
    D = Ef*np.cosh(2*bf*X/lbd)
    return A+B+C+D
    
def dv2_contrib_per_dend_synapse_type2(x, f, Gf2,\
                            syn_freq, Erev, Density, Garray,\
                            soma, stick, Params,
                            precision=1e2):
    [ri, L, D, cm] = stick['ri'], stick['L'], stick['D'], stick['cm']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                                    soma, stick, Params)
    v1 = (V0-v0)/( 1/np.tanh(L/lbd)+(Tau)/(ri*Cm*lbd) )/np.sinh(L/lbd)

    af, bf = split_root_square_of_imaginary(f, tau)
    Aa = (V0-v0)/v1
    Bf2 = (lbd*Cm*ri/Tau)*(lbd*Cm*ri/Tau)*(af*af+bf*bf)
    Cf = Bf2+1
    Df = (lbd*Cm*ri/Tau)*af
    Ef = Bf2-1
    Af2 = 1./(1+(2*np.pi*f*tau)**2) # unitary input for the kernel

    FACTOR = syn_freq*np.pi*D*Density*(af*af+bf*bf)*(tau/cm)**2/lbd*v1*Gf2*Af2
    FACTOR /= sum_expr_ch_sh_sin_cos(L, lbd, af, bf, Cf, Df, Ef)

    print FACTOR
    output = np.zeros(len(f))
    for i in range(len(f)):

        INT1_TERM = Cf[i]*integrate_ch2_cosh(Aa, L/lbd, af[i], 0, L/lbd)
        INT1_TERM += Df[i]*integrate_ch2_sinh(Aa, L/lbd, af[i], 0, L/lbd)
        INT1_TERM += Df[i]*integrate_ch2_sin(Aa, L/lbd, bf[i], 0, L/lbd)
        INT1_TERM += Ef[i]*integrate_ch2_cos(Aa, L/lbd, bf[i], 0, L/lbd)
        INT1_TERM *= (np.cosh(2*af[i]*(x-L)/lbd)+np.cos(2*bf[i]*(x-L)/lbd))

        INT2_TERM=integrate_ch2_cosh(Aa, 0, af[i], (x-L)/lbd, 0)
        INT2_TERM+=integrate_ch2_cos(Aa, 0, bf[i], (x-L)/lbd, 0)

        output[i] = INT1_TERM+INT2_TERM

    return FACTOR*output
