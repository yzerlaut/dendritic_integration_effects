import numpy as np
import matplotlib
import matplotlib.pylab as plt


from matplotlib.ticker import MaxNLocator, FormatStrFormatter

plt.figure(figsize=(5,10))
plt.subplots_adjust(left=.19, bottom=.1, top=.98, right=.95)


ax1 = plt.subplot(511)
plt.ylabel('$\mu_V$ (mV)')

ax2 = plt.subplot(512)
plt.ylabel('$\sigma_V$ (mV)')

ax3 = plt.subplot(513)
plt.ylabel('$\\tau_V$ (ms)')

ax4 = plt.subplot(514)
plt.ylabel('$R_{in}$ (M$\Omega$)')

ax5 = plt.subplot(515)
plt.ylabel('$R_{TF}$ (M$\Omega$)')


# first couple
x, muV_exp, sV_exp, Tv_exp, xth, muV, std_th, Tv_th, Rin, Rtf=\
       np.load('data/fe5_fi20.npy')
ax1.plot(1e6*x, muV_exp, 'bD', label='$\\nu_e$=5Hz, $\\nu_i$=20Hz')
ax1.plot(1e6*xth, 1e3*muV, 'b-', lw=4, alpha=.5)
ax2.plot(1e6*x, sV_exp, 'bD')
ax2.plot(1e6*xth, 1e3*std_th, 'b-', lw=4, alpha=.5)
ax3.plot(1e6*x, 1e3*Tv_exp, 'bD')
ax4.plot(1e6*xth, 1e-6*Rin, 'b-', lw=4, alpha=.5)
ax5.plot(1e6*xth, 1e-6*Rtf, 'b-', lw=4, alpha=.5)
x, muV_exp, sV_exp, Tv_exp, xth, muV, std_th, Tv_th, Rin, Rtf=\
       np.load('data/fe2_fi10.npy')
ax1.plot(1e6*x, muV_exp, 'rD', label='$\\nu_e$=2.5Hz, $\\nu_i$=10Hz')
ax1.plot(1e6*xth, 1e3*muV, 'r-', lw=4, alpha=.5)
ax2.plot(1e6*x, sV_exp, 'rD')
ax2.plot(1e6*xth, 1e3*std_th, 'r-', lw=4, alpha=.5)
ax3.plot(1e6*x, 1e3*Tv_exp, 'rD')
ax4.plot(1e6*xth, 1e-6*Rin, 'r-', lw=4, alpha=.5)
ax5.plot(1e6*xth, 1e-6*Rtf, 'r-', lw=4, alpha=.5)

x, muV_exp, sV_exp, Tv_exp, xth, muV, std_th, Tv_th, Rin, Rtf=\
       np.load('data/fe10_fi10.npy')
ax1.plot(1e6*x, muV_exp, 'gD', label='$\\nu_e$=10Hz, $\\nu_i$=10Hz')
ax1.plot(1e6*xth, 1e3*muV, 'g-', lw=4, alpha=.5)
ax2.plot(1e6*x, sV_exp, 'gD')
ax2.plot(1e6*xth, 1e3*std_th, 'g-', lw=4, alpha=.5)
ax3.plot(1e6*x, 1e3*Tv_exp, 'gD')
ax4.plot(1e6*xth, 1e-6*Rin, 'g-', lw=4, alpha=.5)
ax5.plot(1e6*xth, 1e-6*Rtf, 'g-', lw=4, alpha=.5)

x, muV_exp, sV_exp, Tv_exp, xth, muV, std_th, Tv_th, Rin, Rtf=\
       np.load('data/fe5_fi40.npy')
ax1.plot(1e6*x, muV_exp, 'kD', label='$\\nu_e$=5Hz, $\\nu_i$=40Hz')
ax1.plot(1e6*xth, 1e3*muV, 'k-', lw=4, alpha=.5)
ax2.plot(1e6*x, sV_exp, 'kD')
ax2.plot(1e6*xth, 1e3*std_th, 'k-', lw=4, alpha=.5)
ax3.plot(1e6*x, 1e3*Tv_exp, 'kD')
ax4.plot(1e6*xth, 1e-6*Rin, 'k-', lw=4, alpha=.5)
ax5.plot(1e6*xth, 1e-6*Rtf, 'k-', lw=4, alpha=.5)


ax1.legend(loc='best', prop={'size':'x-small'})
ax1.set_xticklabels([])
ax1.yaxis.set_major_locator( MaxNLocator(nbins = 4) )
ax2.set_xticklabels([])
ax2.yaxis.set_major_locator( MaxNLocator(nbins = 4) )
ax3.set_xticklabels([])
ax3.yaxis.set_major_locator( MaxNLocator(nbins = 4) )
ax3.set_ylim([4.5,6.5])
ax3.yaxis.set_major_locator( MaxNLocator(nbins = 4) )
ax4.set_xticklabels([])
ax4.yaxis.set_major_locator( MaxNLocator(nbins = 4) )
ax5.set_xlabel('distance from soma ($\mu$m)')
ax5.yaxis.set_major_locator( MaxNLocator(nbins = 4) )

# plt.tight_layout()
plt.show()
