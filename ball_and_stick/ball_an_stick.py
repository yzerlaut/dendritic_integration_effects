import numpy as np
import matplotlib
import matplotlib.pylab as plt
import sys
sys.path.append('/home/yann/work/yns_python_library')
import fourier_for_real as rfft
from signanalysis import autocorrel

WITH_PLOT = False # if False, then will save data
window_for_autocorrel = 50e-3 # just window to store the autocorrel

if len(sys.argv)<2:
    fe, fi = 5., 20.
else:
    fe, fi = float(sys.argv[1]), float(sys.argv[2])

# ------- model parameters in SI units ------- # 
params = {
'g_pas': 1e-3*1e4, 'cm' : 1*1e-2, 'Ra' : 35.4*1e-2, 'El': -65e-3,\
'Qe' : 1.e-9 , 'Te' : 5.e-3, 'Ee': 0e-3,\
'Qi' : 1.5e-9 , 'Ti' : 5.e-3, 'Ei': -80e-3,\
'seed' : 0}

FACTOR_FOR_DENSITY = 1. # because Ball & Sticks sucks !!!

# synaptic density  area (m2) per one synapse !!
# ball (soma)
soma = {'L': 40*1e-6, 'D': 20*1e-6, 'NSEG': 1,
        'exc_density':FACTOR_FOR_DENSITY*1e9,
        'inh_density':FACTOR_FOR_DENSITY*25*1e-12,
        'Ke':1e-9, 'Ki':10., 'name':'soma'}
# stick
stick = {'L': 2000*1e-6, 'D': 4*1e-6, 'NSEG': 30,\
         'exc_density':FACTOR_FOR_DENSITY*17*1e-12,
         'inh_density':FACTOR_FOR_DENSITY*100*1e-12,
         'Ke':100, 'Ki':100., 'name':'dend'}

x = np.linspace(0, stick['L'], stick['NSEG'], endpoint=True)
xth = np.linspace(0, stick['L'], 20)

from theory import *
# we calculate the parameters to plug into cable theory
params_for_cable_theory(stick, params)
cables = [soma, stick]

# density of synapses in um2, one synapses per this area !
# so calculate synapse number
Ke_tot, Ki_tot = 0, 0
for i in range(len(cables)):
    cables[i]['Ki_per_seg'] = cables[i]['L']*\
          cables[i]['D']*np.pi/cables[i]['NSEG']/cables[i]['inh_density']
    Ki_tot += cables[i]['Ki_per_seg']*cables[i]['NSEG']
    cables[i]['Ke_per_seg'] = cables[i]['L']*\
          cables[i]['D']*np.pi/cables[i]['NSEG']/cables[i]['exc_density']
    Ke_tot += cables[i]['Ke_per_seg']*cables[i]['NSEG']
print "Total number of EXCITATORY synapses : ", Ke_tot
print "Total number of INHIBITORY synapses : ", Ki_tot

Garray = calculate_mean_conductances(fe, fi, soma, stick, params)

dt, tstop = 0.025, 20000.

from nrn_simulations import *
exc_synapses, exc_netcons, exc_netstims,\
       inh_synapses, inh_netcons, inh_netstims,\
       area_lists, spkout =\
          Constructing_the_ball_and_tree(params, cables,
                                spiking_mech=False)

for i in range(len(cables)):
    for j in range(len(area_lists[i])):
        # excitation
        Ke = cables[i]['Ke_per_seg']
        exc_netstims[i][j].interval = 1e3/fe/Ke
        Ki = cables[i]['Ki_per_seg']
        inh_netstims[i][j].interval = 1e3/fi/Ki

## --- recording
t_vec = nrn.Vector()
t_vec.record(nrn._ref_t)
## --- launching the simulation
nrn.finitialize(params['El']*1e3)
nrn.dt = dt
V = np.zeros((int(tstop/dt),cables[1]['NSEG']))
i=0
while nrn.t<(tstop-dt):
    j = 0
    for seg in nrn.cable_1_0:
        V[i,j] = nrn.cable_1_0(seg.x)._ref_v[0]
        j+=1
    i+=1
    nrn.fadvance()

from scipy.optimize import curve_fit
exp_f = lambda t, tau: np.exp(-t/tau)
def find_acf_time(v_acf, t_shift, criteria=0.01):
    i_max = np.argmin(np.abs(v_acf-criteria))
    P, pcov = curve_fit(exp_f, t_shift[:i_max], v_acf[:i_max])
    return P[0]

muV_exp, sV_exp = V[1000:,:].mean(axis=0), V[1000:,:].std(axis=0)
Tv_exp = 0*muV_exp

i0 = int(200/dt) # start for det of ACF
for i in range(len(muV_exp)):
    v_acf, t_shift = autocorrel(V[i0:,i], window_for_autocorrel, 1e-3*dt)
    Tv_exp[i] = find_acf_time(v_acf, t_shift, criteria=0.01)
print Tv_exp

print '----------------------------------------------------'
print ' Start calculus'
print '----------------------------------------------------'

dt, tstop = 1e-5, 50e-3
t = np.arange(int(tstop/dt))*dt
f = rfft.time_to_freq(len(t), dt)
muV = stat_pot_function(xth, Garray, soma, stick, params)
std_th = np.sqrt(get_the_theoretical_variance(fe, fi, f, xth, params, soma, stick))
Tv_th = 0*muV # TO BE IMPLEMENTED
Rin, Rtf = get_the_input_and_transfer_resistance(fe, fi, f, xth, params, soma, stick)

print Rin, Rtf

print '----------------------------------------------------'
print ' end calculus'
print '----------------------------------------------------'

    
if WITH_PLOT:
    V = np.array(V)
    t = np.array(t_vec)[:-1]
    # membrane pot 
    fig1 = plt.figure()
    plt.errorbar(1e6*x, muV_exp, yerr=sV_exp, color='k', label='num. sim')

    plt.fill_between(1e6*xth, 1e3*(std_th+muV), 1e3*(muV-std_th),\
                 color='k', alpha=.3)
    plt.plot(1e6*xth, 1e3*muV, color='k', lw=3, label='analytical approx')
    plt.legend(loc='best', prop={'size':'small'})
    plt.xlabel('distance from soma ($\mu$m)')
    plt.ylabel('$V_m$ (mV)')
    plt.tight_layout()
    plt.savefig('/home/yann/Desktop/fig1.svg', format='svg')
    
    fig2 = plt.figure()
    plt.title('$\\nu_e$='+str(round(fe))+'Hz, $\\nu_i$='+str(round(fi))+'Hz')
    i1, i2 = 0, int(1000/(t[1]-t[0]))#int(tstop/dt/3.), int(2*tstop/dt/3.)
    plt.plot(1e-3*t[i1:i2], V[i1:i2,0], 'b', label='soma')
    plt.plot(1e-3*t[i1:i2], V[i1:i2,-1], 'r', label='distal')
    plt.plot(1e-3*t[i1:i2], V[i1:i2,int(V.shape[1]/2.)], 'g', label='medial')
    
    plt.xlabel('time (s)')
    plt.ylabel('$V_m$ (mV)')
    plt.legend(loc='best', prop={'size':'small'})
    plt.tight_layout()
    plt.savefig('/home/yann/Desktop/fig2.svg', format='svg')

    # then autocorrelation plots
    fig3 = plt.figure(figsize=(4,3))
    v_acf, t_shift = autocorrel(V[i0:,0], window_for_autocorrel, 1e-3*(t[1]-t[0]))
    plt.plot(1e3*t_shift, v_acf, 'b', label='soma')
    plt.plot(1e3*t_shift, exp_f(t_shift, Tv_exp[0]), 'b--')
    
    v_acf, t_shift = autocorrel(V[i0:,-1], window_for_autocorrel, 1e-3*(t[1]-t[0]))
    plt.plot(1e3*t_shift, v_acf, 'r', label='distal')
    plt.plot(1e3*t_shift, exp_f(t_shift, Tv_exp[-1]), 'r--')
    
    v_acf, t_shift = autocorrel(V[i0:,int(V.shape[1]/2.)],\
                window_for_autocorrel, 1e-3*(t[1]-t[0]))
    plt.plot(1e3*t_shift, v_acf, 'g', label='medial')
    plt.plot(1e3*t_shift, exp_f(t_shift, Tv_exp[int(V.shape[1]/2.)]), 'g--')
    
    plt.xlabel('time (ms)')
    plt.ylabel('ACF (norm.)')
    plt.legend(loc='best', prop={'size':'x-small'})
    plt.tight_layout()
    plt.savefig('/home/yann/Desktop/fig3.svg', format='svg')
    
else:
    # we save the numerical simulations output
     np.save('data/fe'+str(int(fe))+'_fi'+str(int(fi))+'.npy',\
        [x, muV_exp, sV_exp, Tv_exp, xth, muV, std_th, Tv_th, Rin, Rtf])


    
    
    
