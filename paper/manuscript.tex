\input{mydoc.tex} % ALL THE LATEX SETTINGS inside

%%%%% =>>>>>> choose your own color for your comments
\def\alain#1{\textcolor{red}{#1}} 		% alain's comments
\def\yann#1{\textcolor{blue}{#1}} 		% yann's comments

\title{Dendritic integration of synaptic input in the
  \textit{fluctuation-driven} regime}

\author{ Yann Zerlaut\thanks{ Unit\'e de Neurosciences, Information et
    Complexit\'e. Centre National de la Recherche Scientifique.  1
    avenue de la terrasse, 91198 Gif sur Yvette, France } \, \& 
  Alain Destexhe\footnotemark[1] \thanks{Correspondance :
    \href{mailto:zerlaut@unic.cnrs-gif.fr,
      destexhe@unic.cnrs-gif.fr}{$<$lastname$>$@unic.cnrs-gif.fr}} }

\date{\today}

% ----- for the header
\def\shorttitle{Dendritic integration in the
  \textit{fluctuation-driven} regime}

 \def\shortauthor{Zerlaut \&
  Destexhe} \def\shortdate{\today}

\begin{document}
\selectlanguage{english}

\maketitle

\begin{abstract}
  In neocortical cells, synaptic input is spread over the entire
  dendritic tree. In this study, we try to evaluate the consequence of
  this property on synaptic integration in the
  \textit{fluctuation-driven} regime. We extend the calculus presented
  in Kuhn et al. 2004 \cite{Kuhn2004} to a 3 compartment model. We
  then have analytical estimates for the quantities describing the
  membrane potential fluctuations at all points of the
  model. \\
  This could be of great use for the modeling of VSD imaging signals
  because it opens the paths to \textit{mean-field} formalisms with
  non-purely somatic membrane potentials. \\
  We also show that this multi-compartemental feature allows the
  synaptic integration to produce high \textit{shunting} while
  remaining in a physiological range for the somatic input impedance.
  This is a crucial feature to establish the properties of
  self-sustained activity in neural networks.
\end{abstract}

\small
\section{Methods}

\subsection{Three compartments model}
\label{sec:3comp}

The 3 compartment model is identical to the one presented in Destexhe
2001 \cite{destexhe2001}. One Somatic/Proximal, one Basal/Medial and
an Apical/Distal one.\\

We take the same parameters as those calculated in the paper.

\subsection{Synaptic input}
\label{sec:3comp}

The synaptic input is different though. We used simple
\textit{exponential} synapses models for the excitation and
inhibtion. The parameters are : $Q_e$= 1 nS, $Q_i$= 1.2 nS, $\tau_e$= 5
ms, $\tau_i$= 5 ms. \\

We will stimulate those synapses with a generator of Poisson
event. The frequency of this generator will be set by the number of
synapses and a population frequency for each synapse type.\\

To sum up the synapses type by compartment: \\
Soma $\rightarrow$ \{inhibition\} \\
Basal $\rightarrow$ \{inhibition, excitation\} \\
Distal $\rightarrow$ \{inhibition, excitation\}


\section{Results}

\subsection{Matrix formulation of the membrane equations}
\label{sec:matrices}

The membrane equations for the 3 compartments can be written on the
form:
\begin{equation}
  \frac{d}{dt} \, V =  L \cdot V + C + I(V, t)
  \label{eq:memb}
\end{equation}

Where $V = [V_S, V_B , V_D]^{T} $ is the 3 dimensional membrane
potential corresponding to the somatic, basal and distal potentials
respectively.

The expression for the Matrix $L$ and the vector $C$ can be found in
\autoref{sec:expression}.

The vector $I(V,t)$ is also 3 dimensional, it corresponds to the
synaptic current input and/or to an electrode current input.

\subsection{Solution method for time varying input}
\label{sec:solution}

To solve \autoref{eq:memb} in the presence of time varying input, we
first diagonalize the matrix $L$, we have $D = P \cdot L \cdot P^{-1}$
\footnote{i'm not able to proove that the matrix $L$ is
  diagonalizable, for 3*3 matrices, you have very few general theorems
  available, but the numerical diagonalization never gave any problem
  !}. $P$ is the "matrice de changement de bases".\\

Now we define $V' = P^{-1} \cdot V$, $C' = P^{-1} \cdot C$ and $I' =
P^{-1} \cdot I$, we get the equation:
\begin{equation}
  \frac{d}{dt} \, V' =  D \cdot V' + C' + I'(V, t)
  \label{eq:memb-diag}
\end{equation}

As $D$ is diagonal, this corresponds to classic first order
differential equations that we can easily solve. \\

Finally, we go back to the real membrane potentials with $V = P \cdot V'$


\subsection{Mean membrane potential}
\label{sec:stat-vm}

As in Kuhn et al. \cite{Kuhn2004}, we approximate that the mean
membrane potential will be given by the stationary solution to a
constant synaptic input having the value of the mean of the real
synaptic input.\\

Then $ \mu_V = -L^{-1} \cdot C$ \\

This could theoretically be expressed in a closed form, however the
expression would be extremely long\footnote{for example, try to
  calculate the determinant of $L$ !}. The inversion will thus be done
numerically with \texttt{numpy}.\\

This aproximation gives very good results, it can be for example seen
on \autoref{subfig:my-a} or on \autoref{subfig:vm}, how the predicted
$\mu_V$ compares to the effective mean over simulation realisations.

\begin{figure}[htb!]
  \subfloat[][]{
    \includegraphics[width=.9\linewidth]{../figures/basal_current_input_during_fluct}
    \label{subfig:my-a}
  } \\
  \subfloat[][]{
    \includegraphics[width=.9\linewidth]{../figures/input_impedance_for_3_comp}
    \label{subfig:my-b}
  } 
  \caption{\textbf{Transfer resistance of the model in the
      fluctuation-driven regime, simulations and analytical
      solution}.\\
    (\textbf{A}) Current input in the basal compartment in top of
    synaptic input. Shown are the 3 traces of membrane potentials for
    the proximal (black), medial (blue) and distal (red)
    compartment. The thin transparent lines are individual
    realisation, the plain line is the average over 10 trials and the
    large dashed line is the analytical solution (\textbf{B}) Transfer
    resistance between a synaptic entry located at proximal dendrite
    (red), basal dendrite (blue) or distal dendrite (green) and the
    soma. Dots are simulations, lines are analytical predictions. The
    black line represents the mean over the transfer resistance
    weighted by the synaptic densities of the compartments (it's the
    \textit{mean} transfer resistance seen by a \textit{mean} synaptic
    input)}
  \label{fig:a}
\end{figure}

\subsection{Input and transfer resistance}
\label{sec:resist}

To calculate the input resistance and transfer resistance to soma, we
will set $I(V,t)$ in \autoref{eq:memb} to a current step at one point
of the membrane.\\

For example, to study the input resistance and transfer resistance to
the soma of a current input at the basal dendrite, we will set:

\begin{equation}
  \label{eq:i_ex}
  I(V,t) = [0,\,I_0 H(t-t_0),\,0]^T
\end{equation}

The effect of this current in the fluctuation driven regime is
illustrated on \autoref{subfig:my-a} (\texttt{NEURON} simulation and
analytical solution). \\

To get the analytical solution, we applied the method presented in
\autoref{sec:solution}.\\

As we get $\Delta V = V(\infty) - V(t<t_0)$, we can calculate the
input resistance and transfer resistance to soma. We show in
\autoref{subfig:my-b}, the evolution of this resistance as a function
of the background activity (comodulation of excitation and inhibition
as in Kuhn et al \cite{Kuhn2004}).


\subsection{Variance of the membrane potential}
\label{sec:syn-input}

\begin{figure}[h!]
  \centering
  \subfloat[][]{\includegraphics[width=.4\linewidth]{../figures/inh_soma} } 
  \subfloat[][]{\includegraphics[width=.4\linewidth]{../figures/inh_basal} } \\
  \subfloat[][]{\includegraphics[width=.4\linewidth]{../figures/exc_basal} } 
  \subfloat[][]{\includegraphics[width=.4\linewidth]{../figures/inh_distal} } \\[.1cm]
  \subfloat[][]{\includegraphics[width=.4\linewidth]{../figures/exc_distal} } 
  \caption{\textbf{Synaptic under after the constant driving force
      approximation}. We show the PSP traces for the 5 different
    synaptic type considered in this study.}
  \label{fig:syn-input}
\end{figure}

To calculate the variance of the membrane potential, we use the same
strategy than in Kuhn et al. 2004 \cite{Kuhn2004} combined with the
method presented in \autoref{sec:solution}.\\

We make the approximation that the driving force remains constant
within the course of a PSP event (excitatory an inhibitory). For
example if we have a inhibitory event at the distal dendrite we make
the approximation that $(V_D(t) - E_i) \sim cst = \mu_{V_D} -E_i$.

The course of the PSP events for the 3 membrane potentials can be seen
in \autoref{fig:syn-input}. We plotted it for each synapse type. \\

Now we use Campbell's theorem to calculate the variance of the
membrane potential, we have:

\begin{equation}
  \label{eq:variance}
  \sigma_{V_i} = \sqrt{
    \sum_{j \in \{ 5 syn \}} N_j
    \cdot \int_0^\infty \big(PSP_{j\,i}(t)\big)^2 dt
    }
\end{equation}

$\{ 5 syn \}$ corresponds to the set of synapses that has been
previsouly introduced : $\{$ inh soma, inh basal, exc basal, inh
distal, exc distal $\}$. $PSP_{j\,i}(t)$ is the PSP at location $i \in
\{soma, basal, distal \}$ as a response to the synaptic event $j$.\\

We calculate $\sigma_{V_i}$ for the three compartments, $i \in \{soma,
basal, distal \}$. \\

The results of this calculus is presented in \autoref{subfig:vm}, This
gives a very accurate prediction.\\

In addition, we added the 5 conductances to show the accuracy of the
Campbell's theorem in this regime \autoref{subfig:cond}.

\begin{figure}[htb!]
  \subfloat[][]{
    \includegraphics[width=.9\linewidth]{../figures/conductance_comod}
    \label{subfig:cond}
  } \\
  \subfloat[][]{
    \includegraphics[width=.9\linewidth]{../figures/membrane_pot_comod}
    \label{subfig:vm}
  } 
  \caption{\textbf{Accuracy of the analytical description}.
    Comparison of the analytical solution and the 3 compartments
    numerical
    simulations as a response to comodulate input
    ($\nu_e = \alpha \, \nu_i + \nu_e^0$)\\
    (\textbf{A}) The mean and variance of the 5 conductances
    corresponding to the 5 synaptic types presented. We are in a
    regime where Cambell's theorem works very well. (\textbf{B}) Mean
    and variance of the membrane potential at the 3 compartments.}
  \label{fig:vm}
\end{figure}


\subsection{To be done}
\label{sec:tbd}

\begin{itemize}
\item Detailed study of the \textit{shunting}, though visible on
  \autoref{subfig:my-a}
\item Extension to cables !!! this would be very nice...
\end{itemize}


\footnotesize
\bibliographystyle{plain}
\bibliography{/home/yann/work/miscellaneous/refs/Neuroscience}

\onecolumn
\newpage
% \twocolumn
\appendix
\beginsupplement

\section{Expression of the matrices}
\label{sec:expression}


\begin{equation}
  \label{eq:linear_matix}
L = \left[
  \begin{matrix} 
    \frac{-\left(g^{BS}_{A} + g^{S}_{L} + \mu^{S}_{gi}\right)}{C^{S}} &
    \frac{g^{BS}_{A}}{C^{S}} &
    0 \\
    \frac{g^{BS}_{A}}{C^{B}} &
    \frac{-\left( g^{BS}_{A} + g^{DB}_{A} + g^{B}_{L} + \mu^{B}_{ge} +
        \mu^{B}_{gi}\right)}{C^{B}} &
    \frac{g^{DB}_{A}}{C^{B}} \\ 
    0 &
    \frac{g^{DB}_{A}}{C^{D}} &
    \frac{-\left(g^{DB}_{A} + g^{D}_{L} +
        \mu^{D}_{ge} + \mu^{D}_{gi}\right)}{C^{D}} 
  \end{matrix}
\right]
\end{equation}

\quad \\[.2cm]

\begin{equation}
  \label{eq:linear_matix}
C = \left[
  \begin{matrix} 
    \frac{g^{S}_{L} \, E_L + \mu^{S}_{gi} \, E_i }{ C^S (g^{S}_{L} \, E_L + \mu^{S}_{gi}) } \\
     \frac{g^{B}_{L} \, E_L + \mu^{B}_{ge} \, E_e + \mu^{B}_{gi} \, E_i}{
    C^{B} \, (g^{B}_{L} + \mu^{B}_{ge} + \mu^{B}_{gi} ) } \\
    \frac{g^{D}_{L} \, E_L + \mu^{D}_{ge} \, E_e + \mu^{D}_{gi} \, E_i }{
      C^{D} \, (g^{D}_{L} + \mu^{D}_{ge} + \mu^{D}_{gi})}
  \end{matrix}
\right]
\end{equation}


\section{Comparison of the custom-made simulations and NEURON}
\label{sec:comp+nrn_python}

\begin{figure}[h!]
  \centering
  \includegraphics[width=.4\linewidth]{../figures/comp_low_rates_NRN_PYTHON}
  \caption{blabla}
  \label{fig:sup1}
\end{figure}

\end{document}

