\documentclass[11pt,twocolumn]{article}

\usepackage{graphics,graphicx}
\usepackage[utf8]{inputenc}
\usepackage[fleqn]{amsmath}
\usepackage{amssymb,mathenv}
\usepackage[francais,english]{babel}
\usepackage{color}
\definecolor{myblue}{rgb}{.0,.1,.4}
\usepackage{array}
\usepackage[colorlinks=true]{hyperref}
\hypersetup{allcolors = myblue} % to have all the hyperlinks in 1 color
\usepackage{microtype} % Slightly tweak font spacing for aesthetics


\usepackage[twocolumn,textwidth=18cm,columnsep=.8cm,
top=3cm,bottom=3cm]{geometry}
\usepackage[labelfont=bf]{caption}

\usepackage{titlesec} % Allows customization of titles
\renewcommand\thesection{\Roman{section}}
\titleformat{\section}[block]{\Large\bfseries}{\thesection.}{.5em}{} 

%----------------------------------------------------------------------------------------
%	HEADER SECTION
%----------------------------------------------------------------------------------------

\usepackage{fancyhdr} % Headers and footers
\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{\footnotesize 
Dendritic effects stabilizes the AI activity regime
% $\bullet$ Y. Zerlaut \& A. Destexhe
$\bullet$ \today \normalsize } % Custom headerb text
\fancyfoot[C]{\thepage} % Custom footer text

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\vspace{-25mm}
\Large\textbf{
Dendritic interactions shape the gain modulation of the neuronal 
transfer function and impacts the stability of self-sustained activity
in sparse recurrent network
}} % Article title

\vspace{-2mm}
\author{
\normalsize
\textsc{Yann Zerlaut}\thanks{\vspace{1mm}
\normalsize 
\href{mailto:zerlaut@unic.cnrs-gif.fr,destexhe@unic.cnrs-gif.fr}
{$<$lastname$>$@unic.cnrs-gif.fr} \newline % Your email address
Unit\'e de Neurosciences, Information et Complexit\'e \newline
Centre National de la Recherche Scientifique \newline
1 avenue de la terrasse, 91198 Gif sur Yvette, France}
\vspace{-2mm}
}
\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle
\selectlanguage{english}


\begin{abstract}
  \textbf{ In this study, we investigate the effect of dendritic
    morphology on the stationary firing rate of neurons in response to
    co-modulated input. We show numerically that it enhances the
    \textit{gain modulation} in the transfer function curve associated
    to the inhibitory level because of \textit{shunting} being more
    efficient in the dendrites than in an equivalent \textit{single
      compartment} neuron. This has an important consequence for
    network dynamics as it stabilizes the self-consistent activity
    regime in recurrent network. This emerges very naturally by
    pluging the transfer functions into simple models of rate activity
    in neural networks. We confirm (?) those findings with network
    simulations of spatially extended neurons.}
\end{abstract}

\section{Introduction}
\label{sec:intro}

In numerical simulations of recurrent networks, self-sustained
activity can be achieved in the case of conductance
synapses. Nevertheless a weakness of those models is the use of very
high synaptic conductance compared to the physiological one.\\

The existence of this fixed point in the network dynamics is related
to the non-monotonic frequency response to co-modulated
input\cite{Kuhn2004}. Indeed to settle to a fixed point for its firing
rate, the neuron should amplify the input frequency in the initial
range of frequencies and attenuate it for a higher frequency
range. This can be produced with conductance synapses models because
at high level of stimulation (i.e. at high level of conductance), the
membrane \textit{shunts} the input and its effect attenuates with the
level of input.\\

Then the explanation for the need of very high conductances becomes
clear, it is to obtain a high \textit{shunting} that will produce this
\textit{gain modulation}. And in the single compartment case, this
\textit{shunting} is not sufficient with physiological values for the
conductances, this is why they need to be adjusted to much higher
values. \\

In this paper, we show that this issue is very easily solved by
introducing a morphology for the neuron. In this case,
\textit{shunting} happens in the dendrites and is much more efficient
than in the \textit{single compartment} case.



\section{Methods}
\label{sec:methods}

\subsection{Neuronal model}

We introduce the neuronal model presented in \autoref{fig:branches},
this model is built to study the influence of dendritic branching on
the neuronal computation.

We introduce the variable $B$ that codes for the branching degree of
the neuron (see inllustrations in \autoref{fig:branches}).

In the \textit{single compartment} approximation ($B=0$), we forget
about the spatial extent of the neuronal membrane and the membrane
potential can be described by a single voltage value at the soma.

In this paper, the membrane will exhibit 3 features:
passive currents, synaptic currents and a spiking mechanism.\\

The passive properties are described by a simple RC circuit with a
membrane capacitance $C_m$, conductance $g_L$, and a reversal
potential $E_L$. 

\begin{equation}
  \label{eq:single-comp}
  C_m \frac{dV}{dt} = g_L \, \big(E_L - V\big)
  +I^{spk}(V) + I^{syn}(V,t) \quad 
\end{equation}

The spiking mechanism is described by the membrane current
$I^{spk}(V)$, it is described in \autoref{sec:hh}. The synaptic
currents $I^{syn}(V,t)$ are described in \autoref{sec:synapses}.

\subsection{Dendritic tree : cable equation}
\label{sec:cable}

For the membrane properties along the dendritic tree, we use the cable
equation formalism (see Rall 1977 \cite{Rall1977} for a review). 


The cable equation is derived from Kirchoff's law at an intracellular
node :
 \begin{equation}
   \label{eq:cable}
   \frac{1}{r_i} \cdot \frac{\partial^2 v}{\partial x^2} = i_m = 
   c_m \frac{\partial v}{\partial t} + \frac{(v-E_L)}{r_m}
   + \pi D \cdot j^{syn}(x)
 \end{equation}
 where $j^{syn}(x)$ in A.m$^{-2}$ takes into account the dendritic
 synaptic currents (no spiking current on
 the dendritic tree). \\

 In our case, the synaptic current will be of the form:
 \begin{equation}
   \label{eq:syn-currents}
    j^{syn}(x) = g_e(x,t) \, (v - E_e) + g_i(x,t) \, (v - E_i)
 \end{equation}

 where $g_e$ and $g_i$ are conductance densities...


\begin{equation}
\label{eq:fourier-cable}
  \frac{\lambda^2 }{q^2_\omega} \cdot \frac{\partial \hat{v} }{\partial x^2} 
  - \hat{v} = - \frac{R_m}{q^2_\omega} \cdot \hat{j}(x)
\end{equation}
where $ R_m = r_m . \pi D $ is a membrane parameter in $\Omega.m^2$
independent of the diameter of the membrane. $\lambda =
\sqrt{\frac{r_m}{r_i}}$ is the stationary electrotonic constant, and $q_\omega
= \sqrt{1+ i \omega c_m r_m}$ will shorten the \textit{effective} electrotonic
length as $\omega$ raises. \\


\subsection{Equivalent Cylinders}
\label{sec:cable}

To investigate the effect of the dendritic branching structure, we
used a simplification discovered and exploited by Rall (1962
\cite{Rall1962},1977 \cite{Rall1977}). He showed that for a certain
class of dendritic tree, the problem can be reduced to a single
\textit{equivalent cylinder}. Our hypothesis to obtain an
\textit{equivalent cylinder} are a simplified version of
\textit{Rall's rules} : the tree is perfectly symmetric, a parent
branch divides into 2 daughter branches, the distal ends all have
\textit{sealed-end} boundary conditions, the membrane properties
$R_m,C_m,\rho_i$ remains constant along the tree, finally the diameter
of the daughter branches $D_d$ scales as $(D^d)^{\frac{3}{2}} =
\frac{1}{2} (D^p)^{\frac{3}{2}}$ where $D_p$ the diameter of the
parent branch (3/2-power rule of Rall). Examples of those dendritic
trees are showed in \autoref{fig:branches}. \\

We briefly show how can we reduce the tree to an \textit{equivalent
  cylinder}. For the more general derivation and details, see Rall
(1977 \cite{Rall1977}). We index the branches with respect to their
generation by the $k$-index, each generation has $2^k$ branches. The
symmetry of the problem allow us to drop the dependency on the
branches (we hypothesize that the current input and the field will be
the same for all the branches in one generation), therefore we write
the equation
for the mean intracellular potential over branches in generation $k$. \\


Every branch follows the cable equation, the parameters are identical
within each generation as it only depends on $D_k$ the diameter of the
branch. We start by rescaling the space with respect to the
electrotonic constant $\lambda^k = \sqrt{\frac{R_m D_k}{4 \rho_i}}$,
$X = \frac{x}{\lambda^k}$, the function $U,V$ and $J$ are the
extracellular and intracellular potential for the new variable
$X$. For the branch $k$ initiating at $X_0^k$ and finishing at $X_0^{k+1}
= X_0^k + \frac{L^k}{\lambda^k}$, we get :

\begin{equation}
\label{eq:normalized-cable}
  \frac{1}{q^2_\omega} 
  \cdot \frac{\partial \hat{V^k} }{\partial X^2} - \hat{V^k} =  
  - \frac{R_m}{q^2_\omega} \cdot \hat{J}(X)
\end{equation}

With this rescaling, the equations are identical in every branch.\\

At branch points, the intracellular current divides into 2 symmetric
branches, then Kirchoff's law gives (in physical units)  :
\begin{equation}
  \frac{1}{r_i^k} \frac{\partial \hat{v^k}}{\partial x} = 2 \cdot
  \frac{1}{r_i^{k+1}} \frac{\partial \hat{v^{k+1}}}{\partial x} 
  \end{equation}
In rescaled units :
  \begin{equation}
  \frac{1}{r_i^k \lambda^k} \frac{\partial \hat{V^k}}{\partial X} = 2 \cdot
  \frac{1}{r_i^{k+1} \lambda^{k+1}} \frac{\partial \hat{V^{k+1}}}{\partial X}
\end{equation}

With the 3/2-power rule of Rall $(D^{k+1})^{\frac{3}{2}} = \frac{1}{2}
(D^k)^{\frac{3}{2}}$, we have : $r_i^k \lambda^k = r_i^{k+1}
\lambda^{k+1} /2$ and so the intracellular potential has a continuous
derivative at branching point. Another property is the continuity of
the intracellular potential at branch point. \\

Therefore, with the rescaling $ X(x) = \int_0^{x} \frac{dy}{\lambda
  (y)} $ (here $\lambda(y)=\lambda^k$ for $y \in [x^k,x^{k+1}]$) the
second order equations are identical in every branch, and at all
branch points the potential and its derivative are continuous. This
means that the problem is now reduced to a unique differential
equation with the proximal and distal boundary conditions. This is the
\textit{equivalent cylinder} problem. The
diameter of this cylinder is the diameter of the initial branch. \\

Note that in the following, we use the rescaling $X(x)$ to perform the
calculus, but every figure will be plotted in terms of the physical
distance $x$.

\begin{figure}[tb]
  \centering
  \includegraphics[width=\linewidth]{f0.pdf}
  \caption{Increasing the branching of the neuronal model. We
    introduce the variable $B$ that represent the number of
    branches. The branching is symmetric and follows Rall's $3/2$
    rule.}
  \label{fig:branches}
\end{figure}

\subsection{Method of solution}
\label{sec:sol-ce}

In the following, we want to solve the equations for the membrane
patches shown in \autoref{fig:branches}. They represent the coupling
of a lumped impedance soma with one dendritic tree. \\

We will use the fact that the solution for the cable branched to the
soma at one end is the linear combination of the \textit{sealed-end}
solution stimulated by the synaptic currents (\autoref{eq:SE}) and
\textit{clamped-end} solution where the ending impedance is the input
impedance of the soma (\autoref{eq:CI}). If the current input into the
dendritic tree at $X=0$ (the junction with the soma) is
$I^{in}_\omega$, the solution can be written :
\begin{equation}
V_\omega(x) = SE_\omega(X) + \frac{I^{in}_\omega}{r_i \lambda} 
\cdot CI_\omega(X)
\end{equation}

where : 

\begin{equation}
  \label{eq:SE}
  \left\{
  \begin{split}
  \frac{1}{q^2_\omega} & \frac{\partial^2 SE_\omega}{\partial X^2} 
  - SE_\omega = - Z_\omega \cdot J_\omega \\[.2cm]
  & \quad \frac{\partial SE_\omega}{\partial X}|_{0,L} = 0
  \end{split}
  \right.
\end{equation}

\begin{equation}
  \label{eq:CI}
  \left\{
  \begin{split}
    & \qquad \frac{1}{q^2_\omega} \frac{\partial^2 CI_\omega}{\partial X^2} - 
    CI_\omega =0 \\[.2cm]
    &  \frac{\partial CI_\omega}{\partial X}|_{L} = 0 \quad ; \quad
    \frac{\partial CI_\omega}{\partial X}|_{} = -1
  \end{split}
  \right.
\end{equation} \quad \\


We calculate the general solution to this equations using Green
Functions. We consider the case of currents that do not vary spatially
$\partial_x J_\omega = 0$, we obtain\footnote{N.B. $SE(X)$ is a
  voltage and that $CI(X)$ is dimensionless.}  :

\begin{equation}
  \label{eq:SE}
  \begin{split}
  SE(X)  = \frac{R_M \, J_\omega }{\sinh\big(q_\omega L \big)} \,
  &
  \Big(\cosh\big(q_\omega(X - L)\big) \, 
  \sinh\big(q_\omega X \big)  \\[.2cm]
  &  - \cosh\big(q_\omega X\big) 
  \sinh\big(q_\omega (X - L)\big) \, \, \Big)
\end{split}
\end{equation}

\begin{equation}
\label{eq:CI}
  CI(X) = \frac{ \cosh\big(q_\omega(X - L) \big)
  }{q_\omega \sinh\big(q_\omega L\big)}
\end{equation}


\subsection{Hodgkin Huxley model for spiking}
\label{sec:hh}

The firing feature of the neuron in our model is produced by
Hodgkin-Huxley like membrane currents\cite{Hodgkin1952a}. We used a
version modified by Traub et al.
\begin{equation}
  \label{eq:hh}
  I^{spk}(V,t) = g_{Na} \, m^{3} \, h  \, (E_{Na} - V) + 
  g_{K} \,  n^4 \, (E_{K} - V) 
\end{equation}
 The parameters were [...]

 This current is inserted only on the somatic compartment.

\subsection{Synaptic currents}
\label{sec:synapses}

Our aim is to mimick the synaptic input of two neuronal populations
projecting afferents into our target neuron. The target neuron has
$K_{exc}$ excitatory synapses and $K_{inh}$ inhibitory synapses. In
each of the two populations, excitatory and inhibitory, the neurons
have a mean firing rate $\nu_{exc}$ and $\nu_{inh}$ respectively. We
model the arrival synaptic times as Poisson process constructed from
the presynaptic frequencies. The processes are assumed uncorrelated
with each other. For an individual synapse, we obtain a synaptic spike
train defined by the arrival times $\{ t^k \}_{k \in \mathbb{N}}$.

Within the same populations (excitatory or inhibitory) all the
synapses have the same properties, a synaptic event correspond to a
jump of conductance $Q_e$ or $Q_i$ followed by a decay of time
constant $\tau_{exc}$ or $\tau_{inh}$, for the excitation and
inhibition respectively (this is the so-called exponential
conductance-based model of synapses). This increase in conductance is
translated into a current locally on the membrane after multiplication
by the time and space-dependent driving force.


For an individual synapse located at $x$ with reversal potential
$E_{syn}$ with a presynaptic firing frequency $\nu$, this correspond
to the following synaptic input current :
\begin{equation}
\left\{
  \begin{split}
    & I^{syn}_{\nu}(x,t) = g(t)\,\big(E_{syn}-V(x,t)\big) \\[.3cm]
    & \tau_{syn}
    \cdot \frac{d g }{dt} = -g(t) + Q \sum_{\{ t^k_{\nu} \}}
    \delta (t_{\nu}^k-t)
  \end{split}
\right.
\end{equation}

Then we spread the $K_{exc}$ and $K{inh}$ synapses over the membrane
area.

\section{Results}
\label{sec:results}

\subsection{Building comparable models}
\label{sec:comparable-model}

It is not trivial to compare the different models presented in
\autoref{fig:branches} as a given set of synaptic parameters will not
have the same effect at soma because of the change in membrane
properties.

\subsubsection{Reponse to co-modulated input}

We construct a co-modulated input, i.e. we increase the synaptic
stimulation concomitently for the excitation and the inhibition
\cite{Kuhn2004}. We do this by trying to keep the membrane potential
around $-55$mV.

For the single compartment, it is easy to build such a
stimulation\cite{Kuhn2004}, the mean membrane potential is given by:
\begin{equation}
  \label{eq:mean-vm}
  \begin{split}
  \overline{V_m} = & \, \, 
  \frac{g_LE_L+\overline{g_e}E_e+\overline{g_i}E_i}
  {g_L+\overline{g_e}+\overline{g_i}} \\[.2cm]
  & \overline{g_e} = \nu_e \, \tau_e \, Q_e \\
  & \overline{g_i} = \nu_i \, \tau_i \, Q_i
  \end{split}
\end{equation}
Therefore the co-modulated input to bring the neuron around $V_0$ is
the couple ($\nu_e, \nu_i$) verifying:
\begin{equation}
\label{eq:comod}
\left\{
  \begin{split}
  \nu_e = & \frac{g_L (E_L- V_0)}{Q_e\tau_e(V_0-E_e)} \, + \,
  \nu_i \, . \, \frac{Q_i \tau_i
    (E_i-V_0)}{Q_e\tau_e(V_0-E_e)} \quad \\[.2cm]
  & \qquad \qquad \nu_i \in [0,\infty[
  \end{split}
\right.
\end{equation}



\footnotesize
\bibliographystyle{plain}
\bibliography{/home/yann/files/papers/neuro/neuro_database,/home/yann/files/papers/physics/physics_database,/home/yann/files/papers/maths/maths_database}

\end{document}

