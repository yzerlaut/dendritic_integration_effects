import matplotlib.pylab as plt
import numpy as np

nrn_params = {
    # 3 compartment parameters
    'RaS' : 4721.3217,
    'RaB' : 3569.8017,
    'RaD' : 896.21483,
    'LS' : 34.8,
    'LB' : 200.,
    'LD' : 515.,
    'diamS' : 34.8,
    'diamB' : 28.845,
    'diamD' : 6.2056,
    'g_pas' : 4.52e-5,
    # passive properties
    'e_pas' : -70,
    'cm' : 1,
    # synaptic params
    'Qe' : .8*1e-3 ,'Te' : 5.,'Ee':0,
    'Qi' : 1.*1e-3 , 'Ti' : 5.,'Ei':-80,
    # the simulation parameters
    'dt':0.01,
    'tstop':1000.,
    'seed':0
    }

import simulations
import neuron_sim
import theory

params = simulations.transform_nrn_params_to_3comp(nrn_params)

Fe, Fi = 1., 5.5 # input excitatory and inhibitory frequencies
# from those parameters, 
Nsyn_array= simulations.setting_synapse_number_according_to_area(params)
freq_array = simulations.building_differential_frequencies(Fe, Fi, params)


LOC = 'basal'
I0 = 2*0.05 # nA, current input
t0 = 500
t = np.arange(int(params['tstop']/params['dt']))*params['dt']

### analytical solving
muG_array, sG_array = theory.mean_and_var_of_all_Gs(Fe, Fi, Nsyn_array, params)
V0th = theory.get_stationary_V(muG_array, params)


t_analytical = np.linspace(t0,params['tstop'])
Vth = theory.step_current_input(t_analytical, I0, params, muG_array,\
                                loc=LOC)
t_analytical = np.concatenate([[0],t_analytical]) # we just add 0 to have a line
Vth = np.concatenate([V0th.reshape(3,1),Vth], axis=1)


### numerical solving, need to average over different trials,
### because need to explicitely set the synaptic bombardement

SEED=10
Iinput = np.zeros((3, len(t))) # I[loc,time]
# Iinput[1,int(t0/dt):] = np.ones(int((tstop-t0)/dt))*I0 # location set here
V = []
for i in range(SEED):
    t, G, VV = neuron_sim.lauch_simulation_from_frequencies(t, Fe, Fi,\
                            params, seed=i, I=[LOC,I0, t0])
    plt.plot(t, VV[0], 'k-', lw=.5, alpha=.2)
    plt.plot(t, VV[1], 'b-', lw=.5, alpha=.2)
    plt.plot(t, VV[2], 'r-', lw=.5, alpha=.2)
    V.append(VV)
V = np.array(V)

# python simulation
plt.plot(t_analytical, Vth[0], 'k--', lw=3, label='analytical')
plt.plot(t_analytical, Vth[1], 'b--', lw=3)
plt.plot(t_analytical, Vth[2], 'r--', lw=3)
plt.plot(t, V[:,0,:].mean(axis=0), 'k-', label='soma')
plt.plot(t, V[:,1,:].mean(axis=0), 'b-', label='basal')
plt.plot(t, V[:,2,:].mean(axis=0), 'r-', label='distal')

plt.legend(loc='lower right', prop={'size':'x-small'})
plt.xlabel('time (ms)')
plt.ylabel('$V_m$ (mV)')

plt.title('Current input at '+LOC+' dendrite')
plt.tight_layout()
plt.savefig('figures/'+LOC+'_current_input_during_fluct.pdf', format='pdf')

plt.show()


