import numpy as np
import matplotlib.pylab as plt

nrn_params = {
    # 3 compartment parameters
    'RaS' : 4721.3217,
    'RaB' : 3569.8017,
    'RaD' : 896.21483,
    'LS' : 34.8,
    'LB' : 200.,
    'LD' : 515.,
    'diamS' : 34.8,
    'diamB' : 28.845,
    'diamD' : 6.2056,
    'g_pas' : 4.52e-5,
    # passive properties
    'e_pas' : -70,
    'cm' : 1,
    # synaptic params
    'Ne' : 160 , 'Qe' : .7*1e-3 ,'Te' : 10.,'Ee':0,
    'Ni' : 400 , 'Qi' : 3*1.2*1e-3 , 'Ti' : 5.,'Ei':-80,
    # the simulation parameters
    'dt':0.01,
    'tstop':200,
    'seed':0
    }
import simulations
params = simulations.transform_nrn_params_to_3comp(nrn_params)

# we intialize the parameters and synaptic variables

# in the order :
# leak cond., capa, mean exc cond, mean, inh. cond., axial cond.
gLd, Cd, gAdb = params['gLd'], params['Cd'], params['gAdb']
gLb, Cb, gAbs = params['gLb'], params['Cb'], params['gAbs']
gLs, Cs = params['gLs'], params['Cs']
El, Ee, Ei = params['e_pas'], params['Ee'], params['Ei']

mgEd, mgId, mgEb, mgIb, mgIs = 1e-2, 1e-2, 1e-2, 1e-2, 1e-2
muG_array = [mgEd, mgId, mgEb, mgIb, mgIs]
# the equation is of the form : dX = L.X + CST

# constant term : CST, homogeneous to a Voltage over time ! [V/s]
CST = np.array([
    (gLs*El+mgIs*Ei)/Cs,
    (gLb*El+mgEb*Ee+mgIb*Ei)/Cb,
    (gLd*El+mgEd*Ee+mgId*Ei)/Cd
    ])
# Linear term : L, homegenous to a Conductance over time ! [S/s]
L = np.matrix([\
   [ -(gLs+mgIs+gAbs)/Cs, gAbs/Cs, 0],
   [ gAbs/Cb, -(gLb+mgEb+mgIb+gAbs+gAdb)/Cb, gAdb/Cb],
   [ 0, gAdb/Cd,  -(gLd+mgEd+mgId+gAdb)/Cd ]
   ])

# from this we can compute the stationary membrane potential
Vstat = np.dot(-L.I,CST)

print Vstat

