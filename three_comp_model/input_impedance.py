import matplotlib.pylab as plt
import numpy as np

nrn_params = {
    # 3 compartment parameters
    'RaS' : 4721.3217,
    'RaB' : 3569.8017,
    'RaD' : 896.21483,
    'LS' : 34.8,
    'LB' : 200.,
    'LD' : 515.,
    'diamS' : 34.8,
    'diamB' : 28.845,
    'diamD' : 6.2056,
    'g_pas' : 4.52e-5,
    # passive properties
    'e_pas' : -70,
    'cm' : 1,
    # synaptic params
    'Qe' : 1.*1e-3 ,'Te' : 5.,'Ee':0,
    'Qi' : 1.2*1e-3 , 'Ti' : 5.,'Ei':-80,
    # the simulation parameters
    'dt':0.01,
    'tstop':10000.,
    'seed':0
    }

import simulations
import neuron_sim
import theory

params = simulations.transform_nrn_params_to_3comp(nrn_params)

FE0 = 0.14
fith = np.linspace(.1, 20) # COMODULATION !!! with FE0 and (1., 5.5) as a reference
feth = fith*(1.-FE0)/5.5+FE0

Rsoma, Rbasal, Rdistal, Rtf_mean = np.zeros(len(fith)), np.zeros(len(fith)),\
    np.zeros(len(fith)), np.zeros(len(fith))
for i in range(len(fith)):
    R = theory.transfer_resist_to_soma(feth[i], fith[i], params)
    Rsoma[i], Rbasal[i], Rdistal[i], Rtf_mean[i] = R


LOC = 'soma'
I0 = 0.2 # nA, current input
t0 = params['tstop']/2
t = np.arange(int(params['tstop']/params['dt']))*params['dt']
tstop, dt = params['tstop'], params['dt']

import neuron_sim
fi = np.linspace(.1, 20, 20) # COMODULATION !!! with FE0 and (1., 5.5) as a reference
fe = fi*(1.-FE0)/5.5+FE0

Rsim_soma, Rsim_basal, Rsim_distal = np.zeros(len(fi)), np.zeros(len(fi)),\
    np.zeros(len(fi))

for i in range(len(fi)):
    Rsim = []
    for LOC in ['soma', 'basal', 'distal']:
        t, G, VV = neuron_sim.lauch_simulation_from_frequencies(t, fe[i], fi[i],\
                            params, seed=np.random.randint(100),\
                            I=[LOC,I0, t0])
        v0 = VV[0, int(200/dt):int(t0/dt)].mean() # we wait 200 ms
        v1 = VV[0, int((t0+100)/dt):int(tstop/dt)].mean() # we add 100ms
        Rsim.append(abs(v1-v0)/I0)
        plt.clf()
        plt.plot(t, VV[0, :])
        plt.savefig('temp.png', format='png')
    Rsim_soma[i], Rsim_basal[i], Rsim_distal[i] = Rsim

plt.clf()
plt.plot(fith, Rsoma, 'r-', label='$R_{soma}$')    
plt.plot(fith, Rbasal, 'b-', label='$R_{basal -> soma}$')    
plt.plot(fith, Rdistal, 'g-', label='$R_{distal -> soma}$')    
plt.plot(fith, Rtf_mean, 'k-', label='$R^{mean}_{syn -> soma}$', lw=3)
plt.plot(fi, Rsim_soma, 'rD')    
plt.plot(fi, Rsim_basal, 'bD')
plt.plot(fi, Rsim_distal, 'gD')
plt.legend(prop={'size':'small'})
plt.ylabel('Transfer resistance (M$\Omega$)')
plt.xlabel('inh. freq (Hz) with comod.')
plt.tight_layout()
plt.savefig('figures/input_impedance_for_3_comp.pdf', format='pdf')
plt.show()

# plt.plot(fith, Rsoma/Rsoma[0], label='$R_{in}$ soma')    
# plt.plot(fith, Rbasal/Rbasal[0], label='$R_{in}$ basal')    
# plt.plot(fith, Rdistal/Rdistal[0], label='$R_{in}$ distal')    
# plt.plot(fith, Rtf_mean/Rtf_mean[0], 'k-', label='$R_{TF}$ syn', lw=2)
# plt.legend()
# plt.show()


