import matplotlib.pylab as plt
import numpy as np

nrn_params = {
    # 3 compartment parameters
    'RaS' : 4721.3217,
    'RaB' : 3569.8017,
    'RaD' : 896.21483,
    'LS' : 34.8,
    'LB' : 200.,
    'LD' : 515.,
    'diamS' : 34.8,
    'diamB' : 28.845,
    'diamD' : 6.2056,
    'g_pas' : 4.52e-5,
    # passive properties
    'e_pas' : -70,
    'cm' : 1,
    # synaptic params
    'Qe' : 0.8*1e-3 ,'Te' : 5.,'Ee':0,
    'Qi' : 1.*1e-3 , 'Ti' : 5.,'Ei':-80,
    # the simulation parameters
    'dt':0.01,
    'tstop':200,
    'seed':0
    }

import simulations
params = simulations.transform_nrn_params_to_3comp(nrn_params)

Fe, Fi = 1., 5.5 # input excitatory and inhibitory frequencies
# from those parameters, 
Nsyn_array= simulations.setting_synapse_number_according_to_area(params)
freq_array = simulations.building_differential_frequencies(Fe, Fi, params)

from theory import *

muG_array, sG_array = mean_and_var_of_all_Gs(Fe, Fi, Nsyn_array, params)
