import numpy as np
import matplotlib.pylab as plt

nrn_params = {
    # 3 compartment parameters
    'RaS' : 4721.3217,
    'RaB' : 3569.8017,
    'RaD' : 896.21483,
    'LS' : 34.8,
    'LB' : 200.,
    'LD' : 515.,
    'diamS' : 34.8,
    'diamB' : 28.845,
    'diamD' : 6.2056,
    'g_pas' : 4.52e-5,
    # passive properties
    'e_pas' : -70,
    'cm' : 1,
    # synaptic params
    'Ne' : 160 , 'Qe' : .7*1e-3 ,'Te' : 10.,'Ee':0,
    'Ni' : 400 , 'Qi' : 3*1.2*1e-3 , 'Ti' : 5.,'Ei':-80,
    # the simulation parameters
    'dt':0.01,
    'tstop':200,
    'seed':0
    }

import neuron_sim
import simulations
params = simulations.transform_nrn_params_to_3comp(nrn_params)

t = np.arange(int(params['tstop']/params['dt']))*params['dt']


# 3 dimensional Current Input : [0] soma, [1] basal, [2] distal
I = np.zeros((3, len(t))) # I[loc,time]
# 5 dimensional Conductance :
# [0] inh soma, [1] inh basal, [2] exc basal, [3] inh distal, [4] exc distal
G = np.zeros((5, len(t))) # G[loc,time]
G[1] = simulations.convolute_with_exp(t, params['Qi'], params['Ti'], [30, np.inf])
G[4] = simulations.convolute_with_exp(t, params['Qe'], params['Te'], [60, 130, np.inf])
G[0] = simulations.convolute_with_exp(t, params['Qi'], params['Ti'], [90, np.inf])
G[2] = simulations.convolute_with_exp(t, params['Qe'], params['Te'], [120, np.inf])
G[3] = simulations.convolute_with_exp(t, params['Qi'], params['Ti'], [150, np.inf])


# python simulation
V = simulations.three_compartment_simulation(t, G, I, params)
plt.plot(t, V[0], 'k--', lw=3, label='python sim.')
plt.plot(t, V[1], 'k--', lw=3)
plt.plot(t, V[2], 'k--', lw=3)


# NEURON simulation
list_of_netcon, list_of_synapses = neuron_sim.init_nrn_3_comp_sim(nrn_params)
[ncI_S, ncI_B, ncE_B, ncI_D, ncE_D] = list_of_netcon
[synI_S, synI_B, synE_B, synI_D, synE_D] = list_of_synapses
def Events():
    ncI_B.event(30)
    ncE_D.event(60)
    ncI_S.event(90)
    ncE_B.event(120)
    ncE_D.event(130)
    ncI_D.event(150)
t,V = neuron_sim.launch_and_record_NEURON_sim(nrn_params, setup_Events=Events)
plt.plot(t, V[0], 'r-', label='NEURON sim.')
plt.plot(t, V[1], 'r-', t, V[2], 'r-')

plt.title('$V_m$ at the 3 different points ')
plt.legend(prop={'size':'small'})
plt.ylabel('$V_m$ (mV)')
plt.xlabel('time (ms)')
plt.tight_layout()
plt.savefig('figures/comp_low_rates_NRN_PYTHON.pdf', format='pdf')
plt.show()


