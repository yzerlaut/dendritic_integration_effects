import numpy as np

def mean_var_of_exp_shtnz(f, N, Q, T):
    muG, sG = f*N*T*Q*1e-3, np.sqrt(f*N*T/2*1e-3)*Q # T in ms and F in Hz !!!
    return muG, sG

def mean_and_var_of_all_Gs(Fe, Fi, Nsyn_array, params):
    """
    Fe and Fi as an input
    Nsyn_array  =  [Ni_S, Ni_B, Ne_B, Ni_D, Ne_D]
    """
    Q_array = [params['Qi'], params['Qi'], params['Qe'], params['Qi'], params['Qe']]
    T_array = [params['Ti'], params['Ti'], params['Te'], params['Ti'], params['Te']]
    freq_array = [Fi, Fi, Fe, Fi, Fe] # we construct the frequency array
    
    muG_array = [mean_var_of_exp_shtnz(freq_array[i], Nsyn_array[i], Q_array[i],
                                       T_array[i])[0] for i in range(5)] # index 0 
    sG_array = [mean_var_of_exp_shtnz(freq_array[i], Nsyn_array[i], Q_array[i],
                                       T_array[i])[1] for i in range(5)] # index 1
    return muG_array, sG_array
    

def construct_matrix_formulation(muG_array, params):
    """ this function construct the linear and constant operator
    forming the 3D equation : dV/dt = L.V + CST for given
    mean conductances
    (linearized version of the real system of equation -> constant Dr.Force approx)
    
    it returns L and CST """
    
    # translating the parameters
    gLd, Cd, gAdb = params['gLd'], params['Cd'], params['gAdb']
    gLb, Cb, gAbs = params['gLb'], params['Cb'], params['gAbs']
    gLs, Cs = params['gLs'], params['Cs']
    El, Ee, Ei = params['e_pas'], params['Ee'], params['Ei']

    mgIs, mgIb, mgEb, mgId, mgEd = muG_array


    # constant term : CST, homogeneous to a Voltage over time ! [V/s]
    CST = np.array([
        (gLs*El+mgIs*Ei)/Cs,
        (gLb*El+mgEb*Ee+mgIb*Ei)/Cb,
        (gLd*El+mgEd*Ee+mgId*Ei)/Cd
        ])
    # Linear term : L, homegenous to a Conductance over time ! [S/s]
    L = np.matrix([\
                   [ -(gLs+mgIs+gAbs)/Cs, gAbs/Cs, 0],
                   [ gAbs/Cb, -(gLb+mgEb+mgIb+gAbs+gAdb)/Cb, gAdb/Cb],
                   [ 0, gAdb/Cd,  -(gLd+mgEd+mgId+gAdb)/Cd ]
                   ])
    return L, CST
    
def get_stationary_V(muG_array, params):
    L, CST = construct_matrix_formulation(muG_array, params)
    return np.array(np.dot(-L.I,CST))[0] # .I is the inverse in numpy matrices, with dot product

import scipy.linalg as la # linear algebra modulus

def diagonalize(L):
    """
    takes a 2d matrix, and diagonalize it, under the hypothesis that
    the imaginary part is negligible, we remove it !!
    """
    w, P = la.eig(L)
    return np.real(w), np.matrix(P, dtype='float64')


def step_current_input(t, I0, params, muG_array, loc='basal'):
    """
    computes the Voltage responses ot a current input at loc
    """
    t = t-t[0] # the analytical solution is rescaled !
    Vs = get_stationary_V(muG_array, params)
    L, CST = construct_matrix_formulation(muG_array, params) # diagonalized pb
    if loc=='soma':
        It = np.array([1,0,0])*I0/params['Cs']
    elif loc=='basal':
        It = np.array([0,1,0])*I0/params['Cb']
    elif loc=='distal':
        It = np.array([0,0,1])*I0/params['Cd']
    else:
        print "location not recognized"
    #
    # they follow the equation : dU/dt = D.U + C + I
    # with the intial conditions : U0 = -D.I*C
    w, P = diagonalize(L) # diagonalissation, eigenvalus + eigenvectors
    C, I = np.array(np.dot(P.I, CST))[0], np.array(np.dot(P.I, It))[0]
    # So the solutions for the U can be evaluated from the simple 1D equation
    U = np.zeros((3, len(t)))
    U0 = np.array(np.dot(P.I, Vs))[0] # initial condition
    for i in range(3):
        U[i,:] = (U0[i]+(I[i]+C[i])/w[i])*np.exp(w[i]*t)-(I[i]+C[i])/w[i]
    return np.array(P*U)

import simulations

def transfer_resist_to_soma(Fe, Fi, params):
    """
    input : Fe, Fi, params
    returns : 'Rss, Rbs, Rds, Rtf_weighted_by_synapses_number]
    ----------------------------------------------------------------
    computes the transfer resistance to the soma from the different
    input location possibilities (soma, basal, distal)
    The last one is the mean input resistance felt by a mean synapse
    """
    Nsyn= simulations.setting_synapse_number_according_to_area(params)
    muG_array, sG_array = mean_and_var_of_all_Gs(Fe, Fi, Nsyn, params)

    Vs = get_stationary_V(muG_array, params)
    L, CST = construct_matrix_formulation(muG_array, params)
    # then diagonalized pb
    # they follow the equation : dU/dt = D.U + C + I
    # with the intial conditions : U0 = -D.I*C
    w, P = diagonalize(L) # diagonalissation, eigenvalus + eigenvectors
    C = np.array(np.dot(P.I, CST))[0] # constant term 
    U0 = np.array(np.dot(P.I, Vs))[0] # initial condition
    
    # Then, we look at the transfer impedance to the soma from the 3 locations
    
    ItS = np.array([1,0,0])/params['Cs'] # CASE 'soma'
    ItB = np.array([0,1,0])/params['Cb']  # CASE 'basal'
    ItD = np.array([0,0,1])/params['Cd']  # CASE 'distal'
    R = []
    for It in [ItS, ItB, ItD]:
        I, U = np.array(np.dot(P.I, It))[0], np.zeros(3)
        for i in range(3):
            U[i] = -(I[i]+C[i])/w[i]
        R_to_soma = np.array(np.dot(P, U-U0))[0,0]# unit current input, so no division
        R.append(R_to_soma) 

    # finally we compute the transfer resistance weighte by the synapse number
    Rw = Nsyn[0]*R[0]+(Nsyn[1]+Nsyn[2])*R[1]+(Nsyn[3]+Nsyn[4])*R[2]
    Rw /= np.sum(Nsyn)
    R.append(Rw)
    return np.array(R)
    
def setup_syn_input_params(params, Vs, synapse='exc_basal'):
    """ sets the params of the input current It = Qsyn*DV/Cp*It """
    It = np.zeros(3) # input !
    if synapse=='inh_soma':
        It = np.array([1,0,0])
        Tsyn, Qsyn = params['Ti'], params['Qi'] # syn time cst and syn weight
        DV, Cp = params['Ei']-Vs[0], params['Cs'] # driving force and local capa
    elif synapse=='inh_basal':
        It = np.array([0,1,0])
        Tsyn, Qsyn = params['Ti'], params['Qi'] 
        DV, Cp = params['Ei']-Vs[1], params['Cb']
    elif synapse=='exc_basal':
        It = np.array([0,1,0])
        Tsyn, Qsyn = params['Te'], params['Qe'] 
        DV, Cp = params['Ee']-Vs[1], params['Cb']
    elif synapse=='inh_distal':
        It = np.array([0,0,1])
        Tsyn, Qsyn = params['Ti'], params['Qi'] 
        DV, Cp = params['Ei']-Vs[2], params['Cd']
    elif synapse=='exc_distal':
        It = np.array([0,0,1])
        Tsyn, Qsyn = params['Te'], params['Qe'] 
        DV, Cp = params['Ee']-Vs[2], params['Cd']
    else:
        print "synapses type not recognized"
    It = Qsyn*DV/Cp*It
    
    return It, Tsyn, Qsyn, DV, Cp
    
def PSP_event(t, params, muG_array, synapse='exc_basal'):
    """
    computes the psp of an event of type synapse
    """
    Vs = get_stationary_V(muG_array, params)
    L, CST = construct_matrix_formulation(muG_array, params) # diagonalized pb

    It, Tsyn, Qsyn, DV, Cp = setup_syn_input_params(params, Vs,
                        synapse=synapse) # function that 
                        
    # now, we compute the variables of the diagonalized problem
    # they follow the equation : dU/dt = D.U + C + I
    # with the intial conditions : U0 = -D.I*C
    w, P = diagonalize(L) # diagonalissation, eigenvalus + eigenvectors
    C, I = np.array(np.dot(P.I, CST))[0], np.array(np.dot(P.I, It))[0]
    
    # So the solutions for the U can be evaluated from the simple 1D equation
    U = np.zeros((3, len(t)))
    for i in range(3):
        if abs(w[i]+1./Tsyn)>1e-12: # if w[i]==-1/Tsyn
            U[i,:] = -C[i]/w[i] + I[i]/(w[i]+1./Tsyn)*(np.exp(w[i]*t)\
                                        -np.exp(-t/Tsyn))
        else:
            U[i,:] = -C[i]/w[i] + t/Tsyn*I[i]*np.exp(-t/Tsyn)
    
    return np.array(P*U)


def need_integral(b1, b2, b3, d1, d2, d3, d0):
    """
    because we need it in the variance calculus
    it returns the value of the integral of
    ( Sum_i b_i * (exp^{d_i}-exp^{d_0})  )**2
    between 0 and +oo
    """
    terms = []
    terms.append( (b1+b2+b3)**2/(-2*d0) )
    terms.append( -2*(b1+b2+b3)*( -b1/(d0+d1)-b2/(d0+d2)-b3/(d0+d3) ) )
    terms.append( b1**2/(-2*d1) )
    terms.append( b2**2/(-2*d2) )
    terms.append( b3**2/(-2*d3) )
    terms.append( -2*b1*b2/(d1+d2) )
    terms.append( -2*b1*b3/(d1+d3) )
    terms.append( -2*b2*b3/(d2+d3) )
    return np.array(terms).sum()


def Variance_calculus(Fe, Fi, params):
    """
    Calculates the variance
    """
    Nsyn_array= simulations.setting_synapse_number_according_to_area(params)
    freq_array = simulations.building_differential_frequencies(Fe, Fi, params)
    muG_array, sG_array = mean_and_var_of_all_Gs(Fe, Fi, Nsyn_array, params)
    
    Vs = get_stationary_V(muG_array, params)
    L, CST = construct_matrix_formulation(muG_array, params) # diagonalized pb
    w, P = diagonalize(L) # diagonalissation, eigenvalus + eigenvectors

    # we will have to loop over all synapse types !!!
    SYN = ['inh_soma', 'inh_basal', 'exc_basal', 'inh_distal', 'exc_distal']
    i_syn = 0

    sigma_V_contrib = np.zeros((3,5)) # will store the different contributions

    # then we will weight all the contributions by the synapse number
    Nsyn= simulations.setting_synapse_number_according_to_area(params)

    for i_syn in range(len(SYN)):
        # we set up the input parameters
        It, Tsyn, Qsyn, DV, Cp = setup_syn_input_params(params, Vs,
                        synapse=SYN[i_syn]) # function that 
        C, I = np.array(np.dot(P.I, CST))[0], np.array(np.dot(P.I, It))[0]
        d0 = -1./Tsyn
        # then we solve the variance contrib for each 
        for i in range(3): # loop over S, B, D to undiagonalize
                b1 = P[i,0]*I[0]/(w[0]+1./Tsyn)
                b2 = P[i,1]*I[1]/(w[1]+1./Tsyn)
                b3 = P[i,2]*I[2]/(w[2]+1./Tsyn)
                sigma_V_contrib[i, i_syn] = need_integral(b1, b2, b3,
                                w[0], w[1], w[2], d0)*freq_array[i_syn]*1e-3

    return np.sqrt(np.sum(sigma_V_contrib, axis=1))



    
    
    
    
