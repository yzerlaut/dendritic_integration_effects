from neuron import h as nrn
import numpy as np
import matplotlib.pylab as plt

nrn('create soma') 
nrn('create dend_B') # basal dendrites compartment
nrn('create dend_D') # distal dendrites compartment


def init_nrn_3_comp_sim(params, list_of_netstim=[None for i in range(5)]):
    """
    initialize a 3 compartment neuron with exponential synapses
    
    list_of_netstim = [nsI_S, nsI_B, nsE_B, nsI_D, nsE_D]
    """

    [nsI_S, nsI_B, nsE_B, nsI_D, nsE_D] = list_of_netstim
    [ncI_S, ncI_B, ncE_B, ncI_D, ncE_D] = [None for i in range(5)] # initialized
    [synI_S, synI_B, synE_B, synI_D, synE_D] = [None for i in range(5)] # initialized
    
    ## SOMATIC AND PROXIMAL COMPARTMENT
    nrn.soma.L, nrn.soma.diam, = params['LS'], params['diamS']
    nrn.soma.nseg, nrn.soma.Ra = 1, params['RaS']
    synI_S= nrn.ExpSyn(0.5, sec=nrn.soma) # inhibitory somatic synapses
    synI_S.e, synI_S.tau =params['Ei'], params['Ti']
    ncI_S= nrn.NetCon(nsI_S, synI_S)
    ncI_S.weight[0]=params['Qi']
    
    ## BASAL COMPARTMENT
    if params['LB']<>0:
        nrn.dend_B.L, nrn.dend_B.diam, = params['LB'], params['diamB']
        nrn.dend_B.nseg, nrn.dend_B.Ra = 1, params['RaB']
        synE_B= nrn.ExpSyn(0.5, sec=nrn.dend_B) # excitatory basal synapses
        synE_B.e, synE_B.tau =params['Ee'], params['Te']
        synI_B= nrn.ExpSyn(0.5, sec=nrn.dend_B) # inhibitory basal synapses
        synI_B.e, synI_B.tau =params['Ei'], params['Ti']
        ncI_B= nrn.NetCon(nsI_B, synI_B)
        ncI_B.weight[0]=params['Qi']
        nrn('soma connect dend_B(0.), 1.')
    else: # in the single compartment case, we put the exc on the soma
        synE_B= nrn.ExpSyn(0.5, sec=nrn.soma) # excitatory basal synapses
        synE_B.e, synE_B.tau =params['Ee'], params['Te']
    # in both cases
    ncE_B= nrn.NetCon(nsE_B, synE_B)
    ncE_B.weight[0]=params['Qe']

    ## DISTAL COMPARTMENT
    if params['LD']<>0:
        nrn.dend_D.L, nrn.dend_D.diam, = params['LD'], params['diamD']
        nrn.dend_D.nseg, nrn.dend_D.Ra = 1, params['RaD']
        synE_D= nrn.ExpSyn(0.5, sec=nrn.dend_D) # excitatory distal synapses
        synE_D.e, synE_D.tau =params['Ee'], params['Te']
        ncE_D= nrn.NetCon(nsE_D, synE_D)
        ncE_D.weight[0]=params['Qe']
        synI_D= nrn.ExpSyn(0.5, sec=nrn.dend_D) # inhibitory distal synapses
        synI_D.e, synI_D.tau =params['Ei'], params['Ti']
        ncI_D= nrn.NetCon(nsI_D, synI_D)
        ncI_D.weight[0]=params['Qi']
        nrn('dend_B connect dend_D(0.),1.')
        print '-----> triple compartment simulation'
    else:
        if params['LB']<>0:
            print '-----> doule compartment simulation'
        else:
            print '-----> single copmartment simulation'

    # then common passive properties
    for sec in nrn.allsec():
        sec.insert('pas')
        sec.g_pas = params['g_pas']
        sec.e_pas = params['e_pas']
        sec.cm = params['cm']
    
    # # spiking mechanism !
    # if params['model']=='iaf':
    #     nrn("""
    #     thresh_SpikeOut = %(Vthre)f //mV
    #     refrac_SpikeOut = %(Trefrac)f //ms
    #     vrefrac_SpikeOut = %(Vreset)f // mV
    #     grefrac_SPikeOut = 0 //us
    #     """ % params)
    #     nrn('objref spk, spikevec, tobj, nil')
    #     nrn('soma spk = new SpikeOut(0.5)')
    #     nrn('spikevec = new Vector()')
    #     nrn('tobj = new NetCon(spk, nil)')
    #     nrn('tobj.record(spikevec)')

    list_of_netcon = [ncI_S, ncI_B, ncE_B, ncI_D, ncE_D]
    list_of_synapses = [synI_S, synI_B, synE_B, synI_D, synE_D]
    return list_of_netcon, list_of_synapses


def launch_and_record_NEURON_sim(params,\
                setup_Events=None, list_of_synapses=None):

    V = []
    Vs = nrn.Vector()
    Vs.record(nrn.soma(0.5). _ref_v)
    V.append(Vs)
    if params['LB']<>0:
        Vb = nrn.Vector()
        Vb.record(nrn.dend_B(0.5). _ref_v)
        V.append(Vb)
    if params['LD']<>0:
        Vd = nrn.Vector()
        Vd.record(nrn.dend_D(0.5). _ref_v)
        V.append(Vd)

    G = []
    if list_of_synapses is not None:
        for syn in list_of_synapses:
            g = nrn.Vector()
            g.record(syn._ref_g)
            G.append(g)

            
    T = nrn.Vector() 
    T.record(nrn._ref_t)
    
    nrn.dt = params['dt']

    if setup_Events is not None:
        fih = nrn.FInitializeHandler(setup_Events)
    nrn.finitialize(params['e_pas'])
    while nrn.t<params['tstop']:
        nrn.fadvance()

    # fout = 1e3*nrn.spikevec.size()/nrn.t
    print "-----------------------------------"
    print "time step : ", nrn.dt
    print "simulation stop : ", nrn.t
    print "-----------------------------------"
    # we restrict our analysis to the stationary behaviour, no transients
    tstop, dt = params['tstop'], params['dt']
    if len(G)>1:
        return np.array(T), np.array(G), np.array(V)
    else:
        return np.array(T), np.array(V)


import simulations

def lauch_simulation_from_frequencies(t, fe, fi, params, seed=0, I=None):

    freq_array = simulations.building_differential_frequencies(fe, fi, params)
    Section = [nrn.soma, nrn.dend_B, nrn.dend_B, nrn.dend_D, nrn.dend_D]
    
    list_of_netstim = []
    for i in range(5):
        ns= nrn.NetStim(0.5, sec=Section[i])
        ns.noise, ns.number= 1, 1e20
        ns.interval = 1e3/(freq_array[i])
        ns.seed(seed+i) # very important : seed
        list_of_netstim.append(ns)

    list_of_netcon, list_of_synapses = \
      init_nrn_3_comp_sim(params, list_of_netstim=list_of_netstim)

    if I is not None:
        nrn('objref ic')
        loc, I0, t0 = I
        if loc=='soma':
            nrn('soma ic = new IClamp(.5)')
        elif loc=='basal':
            nrn('dend_B ic = new IClamp(.5)')
        elif loc=='distal':
            nrn('dend_D ic = new IClamp(.5)')
        nrn.ic.amp, nrn.ic.delay, nrn.ic.dur = I0, t0, 1e6
        
    t, G, V = launch_and_record_NEURON_sim(params, list_of_synapses=\
                                           list_of_synapses)    
    return t, G, V


if __name__ == '__main__':
    # we generate an example with controlled spikes
    params = {
        # 3 compartment parameters
        'RaS' : 4721.3217,
        'RaB' : 3569.8017,
        'RaD' : 896.21483,
        'LS' : 34.8,
        'LB' : 200.,
        'LD' : 515.,
        'diamS' : 34.8,
        'diamB' : 28.845,
        'diamD' : 6.2056,
        'g_pas' : 4.52e-5,
        # passive properties
        'e_pas' : -70,
        'cm' : 1,
        # synaptic params
        'Ne' : 160 , 'Qe' : .7*1e-3 ,'Te' : 10.,'Ee':0,
        'Ni' : 400 , 'Qi' : 3*1.2*1e-3 , 'Ti' : 5.,'Ei':-80,
        # the simulation parameters
        'dt':0.01,
        'tstop':200,
        'seed':0
        }
    list_of_netcon, list_of_synapses = init_nrn_3_comp_sim(params)
    [ncI_S, ncI_B, ncE_B, ncI_D, ncE_D] = list_of_netcon
    [synI_S, synI_B, synE_B, synI_D, synE_D] = list_of_synapses
    def Events():
        ncI_B.event(30)
        ncE_D.event(60)
        ncI_S.event(90)
        ncE_B.event(120)
        ncE_D.event(130)
        ncI_D.event(150)
    t,V = launch_and_record_NEURON_sim(params, setup_Events=Events)
    plt.plot(t, V[0], label='soma')
    plt.plot(t, V[1], label='basal')
    plt.plot(t, V[2], label='distal')
    plt.show()
