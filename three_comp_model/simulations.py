"""
exceptionnally, because of the limited precision in the linalg modulus
we will work with the system unit :
[mV, nA, uS, MOhm, nF, ms]
"""

import numpy as np


def convolute_with_exp(t, Q, Tsyn, spike_events, g0=0):
    g = np.ones(t.size)*g0 # init to first value
    dt, t = t[1]-t[0], t-t[0] # we need to have t starting at 0
    # stupid implementation of a shotnoise
    event = 0 # index for the spiking events
    for i in range(1,t.size):
        g[i] = g[i-1]*np.exp(-dt/Tsyn)
        while spike_events[event]<=t[i]:
            g[i]+=Q
            event+=1
    return g

    
def generate_conductance_shotnoise(freq, t, N, Q, Tsyn, g0=0, seed=0):
    """
    generates a shotnoise convoluted with a waveform
    frequency of the shotnoise is freq,
    K is the number of synapses that multiplies freq
    g0 is the starting value of the shotnoise
    """
    if freq==0:
        print "problem, 0 frequency !!! ---> freq=1e-9 !!"
        freq=1e-9
    upper_number_of_events = max([int(3*freq*t[-1]*N),1]) # at least 1 event
    np.random.seed(seed=seed)
    spike_events = np.cumsum(np.random.exponential(1./(N*freq),\
                             upper_number_of_events))

    return convolute_with_exp(t, Q, Tsyn, spike_events, g0=g0)



def transform_nrn_params_to_3comp(params):
    """
    transform NEURON membrane specific paramters into
    real circuit parameters, the unit system is :
    [mV, nA, uS, MOhm, nF, ms]
    """
    
    # we start with the half segment intracellular resistance
    RaS_1_2 = .01*params['RaS']*(params['LS']/2.)/(np.pi*(params['diamS']/2.)**2) # MegaOhms
    RaB_1_2 = .01*params['RaB']*(params['LB']/2.)/(np.pi*(params['diamB']/2.)**2) # MegaOhms
    RaD_1_2 = .01*params['RaD']*(params['LD']/2.)/(np.pi*(params['diamD']/2.)**2) # MegaOhms
    # to get the axial conductance
    if (RaD_1_2<>0) and (RaB_1_2<>0):
        params['gAdb'] = 1./(RaD_1_2+RaB_1_2) # microSiemens
    else:
        params['gAdb'] = np.inf
    if (RaS_1_2<>0) and (RaB_1_2<>0):
        params['gAbs'] = 1./(RaB_1_2+RaS_1_2) # microSiemens
    else:
        params['gAbs'] = np.inf

    # then membrane conductance
    params['gLs'] = 1e-2*np.pi*params['LS']*params['diamS']*params['g_pas'] # microSiemens
    params['gLb'] = 1e-2*np.pi*params['LB']*params['diamB']*params['g_pas'] # microSiemens
    params['gLd'] = 1e-2*np.pi*params['LD']*params['diamD']*params['g_pas'] # microSiemens

    # then membrane capacitance
    params['Cs'] = 1e-5*np.pi*params['LS']*params['diamS']*params['cm'] # nanoFarad
    params['Cb'] = 1e-5*np.pi*params['LB']*params['diamB']*params['cm'] # nanoFarad
    params['Cd'] = 1e-5*np.pi*params['LD']*params['diamD']*params['cm'] # nanoFarad

    return params

def three_compartment_simulation(t, G, I, params, v0=0):
    """
    simulation of a 3 compartment model

    # 5 dimensional Conductance :
    # [0] inh soma, [1] inh basal, [2] exc basal, [3] inh distal, [4] exc distal
    
    # 3 dimensional Current Input : [0] soma, [1] basal, [2] distal
    I = np.zeros((3, len(t))) # I[loc,time]
    """

    Ee, Ei, El = params['Ee'], params['Ei'], params['e_pas']
    # leak conductance
    gLs, gLb, gLd = params['gLs'], params['gLb'], params['gLd']
    # capacitance
    Cs, Cb, Cd = params['Cs'], params['Cb'], params['Cd']
    # intracellular conductance
    gAbs, gAdb = params['gAbs'], params['gAdb']

    if (Cb<>0) and (Cd<>0): # if real triple compartment !
        def diff_operator(v, g, i):
            [gIs, gIb, gEb, gId, gEd] = g
            # takes the conductance and computes the temporal derivative of V
            dVs = (gLs*(El-v[0])+gIs*(Ei-v[0])-gAbs*(v[0]-v[1])+i[0])/Cs
            dVb = (gLb*(El-v[1])+gIb*(Ei-v[1])+gEb*(Ee-v[1])+gAbs*(v[0]-v[1])-gAdb*(v[1]-v[2])+i[1])/Cb
            dVd = (gLd*(El-v[2])+gId*(Ei-v[2])+gEd*(Ee-v[2])+gAdb*(v[1]-v[2])+i[2])/Cd
            return np.array([dVs, dVb, dVd])
    elif Cb<>0: # then onlly Cd<>0, so two compartment
        def diff_operator(v, g, i):
            [gIs, gIb, gEb, _, _] = g
            # takes the conductance and computes the temporal derivative of V
            dVs = (gLs*(El-v[0])+gIs*(Ei-v[0])-gAbs*(v[0]-v[1])+i[0])/Cs
            dVb = (gLb*(El-v[1])+gIb*(Ei-v[1])+gEb*(Ee-v[1])+gAbs*(v[0]-v[1])+i[1])/Cb
            return np.array([dVs, dVb, dVb]) # basal nd distal are isopotential
    else: # single compartment
        def diff_operator(v, g, i):
            [gIs, _, gEb, _, _] = g # the basal exc is put on the soma
            # takes the conductance and computes the temporal derivative of V
            dVs = (gLs*(El-v[0])+gIs*(Ei-v[0])+gEb*(Ee-v[0])+i[0])/Cs
            return np.array([dVs, dVs, dVs]) # all 2 are ispotential
        
    # 3 dimensional Voltage : [0] soma, [1] basal, [2] distal
    V = np.ones((3, len(t)))*params['e_pas'] # V[loc,time]
    dt = t[1]-t[0]

    for i in range(len(t)-1):

        V[:,i+1] = V[:,i] + dt*diff_operator(V[:,i], G[:,i], I[:,i])

    return V

def setting_synapse_number_according_to_area(params, synaptic_parameters = {\
                'unit_memb_area_per_exc_synapse': 17., # um2, one synapses per this area !
                'unit_dendritic_memb_area_per_inh_synapse': 100., # um2
                'unit_somatic_memb_area_per_inh_synapse': 25., # um2
                'unit_axonal_memb_area_per_inh_synapse': 17. # um2
                }):
    # excitation
    params['Ne_B'] = np.pi*params['LB']*params['diamB']\
        /synaptic_parameters['unit_memb_area_per_exc_synapse']
    params['Ne_D'] = np.pi*params['LD']*params['diamD']\
        /synaptic_parameters['unit_memb_area_per_exc_synapse']

    # inhibition
    params['Ni_S'] = np.pi*params['LS']*params['diamS']\
        /synaptic_parameters['unit_somatic_memb_area_per_inh_synapse']
    params['Ni_B'] = np.pi*params['LB']*params['diamB']\
        /synaptic_parameters['unit_dendritic_memb_area_per_inh_synapse']
    params['Ni_D'] = np.pi*params['LD']*params['diamD']\
        /synaptic_parameters['unit_dendritic_memb_area_per_inh_synapse']
    return [params['Ni_S'], params['Ni_B'], params['Ne_B'],
            params['Ni_D'], params['Ne_D']]

    print """
    SYNAPSES :
    ------> inhibitory :
    --------------------- proximal dendrites : N = %(Ni_S)f
    --------------------- basal dendrites : N = %(Ni_B)f
    --------------------- distal dendrites : N = %(Ni_D)f
    ------> excitatory :
    --------------------- basal dendrites : N = %(Ne_B)f
    --------------------- distal dendrites : N = %(Ne_D)f
    """ %(params)


def building_differential_frequencies(fe, fi, params):
    """
    takes two excitatry and inhibitory frequencies and
    returns the absolute frequencies according to the number
    of synapses :
   [fi_S, fi_B, fe_B, fi_D, fe_D]
    """
    return np.array([
        fi*params['Ni_S'],
        fi*params['Ni_B'],
        fe*params['Ne_B'],
        fi*params['Ni_D'],
        fe*params['Ne_D']
        ])


def lauch_simulation_from_frequencies(t, fe, fi, params, seed=0, I=None):

    freq_array = building_differential_frequencies(fe, fi, params)
    Q_array = [params['Qi'], params['Qi'], params['Qe'], params['Qi'], params['Qe']]
    T_array = [params['Ti'], params['Ti'], params['Te'], params['Ti'], params['Te']]
    
    if I is None: # if no particular additional current input
        I = np.zeros((3, len(t))) # I[loc,time]
        
    G = np.zeros((5, len(t))) # G[loc,time], created
    # then we fill it !!
    for ig in range(len(G[:,0])):
        G[ig] = generate_conductance_shotnoise(
            freq_array[ig], t, 1, Q_array[ig], T_array[ig], seed=seed)
    # now we run the simulation
    V = three_compartment_simulation(t, G, I, params)
    return G, V
