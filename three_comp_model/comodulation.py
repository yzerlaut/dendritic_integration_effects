import matplotlib.pylab as plt
import numpy as np

nrn_params = {
    # 3 compartment parameters
    'RaS' : 4721.3217,
    'RaB' : 3569.8017,
    'RaD' : 896.21483,
    'LS' : 34.8,
    'LB' : 200.,
    'LD' : 515.,
    'diamS' : 34.8,
    'diamB' : 28.845,
    'diamD' : 6.2056,
    'g_pas' : 4.52e-5,
    # passive properties
    'e_pas' : -70,
    'cm' : 1,
    # synaptic params
    'Qe' : .8*1e-3 ,'Te' : 5.,'Ee':0,
    'Qi' : 1.*1e-3 , 'Ti' : 5.,'Ei':-80,
    # the simulation parameters
    'dt':0.01,
    'tstop':10000,
    'seed':0
    }

import simulations
import neuron_sim
import theory


params = simulations.transform_nrn_params_to_3comp(nrn_params)
t = np.arange(int(params['tstop']/params['dt']))*params['dt']
# then synapses number !!
Nsyn_array= simulations.setting_synapse_number_according_to_area(params)

FE0 = 0.14
fiSim = np.linspace(.1, 20, 5) # COMODULATION !!! with FE0 and (1., 5.5) as a reference
feSim = fiSim*(1.-FE0)/5.5+FE0


f1 = plt.figure(figsize=(7,5)) # membrane potentials
ax1 = plt.subplot(111)
plt.tight_layout()

f2 = plt.figure(figsize=(7,5)) # conductances
ax2 = plt.subplot(111)
plt.tight_layout()

DECAL = .1 # to shift the 3 plots
for i in range(len(fiSim)):

    # simulation
    freq_array = simulations.building_differential_frequencies(\
                        feSim[i], fiSim[i], params)
    t, G, V = neuron_sim.lauch_simulation_from_frequencies(t, feSim[i], fiSim[i],\
                            params)
    G = 1e3*G
    # membrane potentials
    mV_s, sV_s = V[0, int(len(t)/10.):].mean(), V[0, int(len(t)/10.):].std()
    mV_b, sV_b = V[1, int(len(t)/10.):].mean(), V[1, int(len(t)/10.):].std()
    mV_d, sV_d = V[2, int(len(t)/10.):].mean(), V[2, int(len(t)/10.):].std()
    ax1.errorbar([fiSim[i]-DECAL], [mV_s], [sV_s], color='k', marker='D', ms=5)
    ax1.errorbar([fiSim[i]], [mV_b], [sV_b], color='b', marker='D', ms=5)
    ax1.errorbar([fiSim[i]+DECAL], [mV_d], [sV_d], color='r', marker='D', ms=5)

    # conductances
    mgi_s, sgi_s = G[0, int(len(t)/10.):].mean(), G[0, int(len(t)/10.):].std()
    mgi_b, sgi_b = G[1, int(len(t)/10.):].mean(), G[1, int(len(t)/10.):].std()
    mge_b, sge_b = G[2, int(len(t)/10.):].mean(), G[2, int(len(t)/10.):].std()
    mgi_d, sgi_d = G[3, int(len(t)/10.):].mean(), G[3, int(len(t)/10.):].std()
    mge_d, sge_d = G[4, int(len(t)/10.):].mean(), G[4, int(len(t)/10.):].std()

    ax2.errorbar([fiSim[i]-2*DECAL], [mgi_s], [sgi_s], color='k', marker='D', ms=5)
    ax2.errorbar([fiSim[i]-DECAL], [mgi_b], [sgi_b], color='r', marker='D', ms=5)
    ax2.errorbar([fiSim[i]], [mge_b], [sge_b], color='g', marker='D', ms=5)
    ax2.errorbar([fiSim[i]+DECAL], [mgi_d], [sgi_d], color='y', marker='D', ms=5)
    ax2.errorbar([fiSim[i]+2*DECAL], [mge_d], [sge_d], color='b', marker='D', ms=5)


# theory
fith = np.linspace(.1, 20, 1e2) # COMODULATION !!! with FE0 and (1., 5.5) as a reference
feth = fith*(1.-FE0)/5.5+FE0
# membrane pot
mV_s, mV_b, mV_d = np.zeros(len(fith)), np.zeros(len(fith)), np.zeros(len(fith))
sV_s, sV_b, sV_d = np.zeros(len(fith)), np.zeros(len(fith)), np.zeros(len(fith))
# conductances
mgi_s, mgi_b, mgi_d = np.zeros(len(fith)), np.zeros(len(fith)), np.zeros(len(fith))
mge_b, mge_d = np.zeros(len(fith)), np.zeros(len(fith))
sgi_s, sgi_b, sgi_d = np.zeros(len(fith)), np.zeros(len(fith)), np.zeros(len(fith))
sge_b, sge_d = np.zeros(len(fith)), np.zeros(len(fith))

for i in range(len(fith)):
    muG_array, sG_array = theory.mean_and_var_of_all_Gs(\
                    feth[i], fith[i], Nsyn_array, params)
    mgi_s[i], mgi_b[i], mge_b[i], mgi_d[i], mge_d[i] = 1e3*np.array(muG_array)
    sgi_s[i], sgi_b[i], sge_b[i], sgi_d[i], sge_d[i] = 1e3*np.array(sG_array)

    V = theory.get_stationary_V(muG_array, params)
    mV_s[i], mV_b[i], mV_d[i] = V
    sV = theory.Variance_calculus(feth[i], fith[i], params)
    sV_s[i], sV_b[i], sV_d[i] = sV
    
ax1.plot(fith, mV_s, color='k', lw=3, alpha=.3, label='soma')
ax1.fill_between(fith, mV_s+sV_s, mV_s-sV_s, color='k', alpha=.15)
ax1.plot(fith, mV_b, color='b', lw=3, alpha=.3, label='medial')
ax1.fill_between(fith, mV_b+sV_b, mV_b-sV_b, color='b', alpha=.15)
ax1.plot(fith, mV_d, color='r', lw=3, alpha=.3, label='distal')
ax1.fill_between(fith, mV_d+sV_d, mV_d-sV_d, color='r', alpha=.15)
ax1.set_xlabel('inh. freq (Hz) with comod.')
ax1.set_ylabel('$V_m$ (mV)')


ax2.plot(fith, mgi_s, color='k', lw=3, alpha=.3, label='$g_i^{S}$')
ax2.fill_between(fith, mgi_s+sgi_s, mgi_s-sgi_s, color='k', alpha=.15)
ax2.plot(fith, mgi_b, color='r', lw=3, alpha=.3, label='$g_i^{B}$')
ax2.fill_between(fith, mgi_b+sgi_b, mgi_b-sgi_b, color='r', alpha=.15)
ax2.plot(fith, mge_b, color='g', lw=3, alpha=.3, label='$g_e^{B}$')
ax2.fill_between(fith, mge_b+sge_b, mge_b-sge_b, color='g', alpha=.15)
ax2.plot(fith, mgi_d, color='y', lw=3, alpha=.3, label='$g_i^{D}$')
ax2.fill_between(fith, mgi_d+sgi_d, mgi_d-sgi_d, color='y', alpha=.15)
ax2.plot(fith, mge_d, color='b', lw=3, alpha=.3, label='$g_e^{D}$')
ax2.fill_between(fith, mge_d+sge_d, mge_d-sge_d, color='b', alpha=.15)
ax2.set_xlabel('inh. freq (Hz) with comod.')
ax2.set_ylabel('conductance (nS)')



ax1.legend(loc='lower right', prop={'size':'small'})
ax2.legend(loc='upper left', prop={'size':'x-small'})
plt.tight_layout()

f1.savefig('figures/membrane_pot_comod.pdf', format='pdf')
f2.savefig('figures/conductance_comod.pdf', format='pdf')

plt.show()
