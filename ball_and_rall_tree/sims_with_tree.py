import numpy as np
import matplotlib
import matplotlib.pylab as plt

# ------- model parameters in SI units ------- # 
params = {
'g_pas': 5e-5*1e4, 'cm' : 1*1e-2, 'Ra' : 35.4*1e-2, 'El': -65e-3,\
'Qe' : 1.e-9 , 'Te' : 5.e-3, 'Ee': 0e-3,\
'Qi' : 1.e-9 , 'Ti' : 5.e-3, 'Ei': -80e-3,\
'exc_density_dend':17*1e-12, 'inh_density_dend':100*1e-12,
'exc_density_soma':1e9, 'inh_density_soma':25*1e-12,
'seed' : 0}

# synaptic density  area (m2) per one synapse !!
# ball (soma)
soma = {'L': 40*1e-6, 'D': 20*1e-6, 'NSEG': 1, 'name':'soma'}
# stick
La, Da = 1000*1e-6, 4*1e-6
# EqCylinder = np.array([0.,La/3.,2*La/3.,La])
EqCylinder = np.array([0.,La/12.,La/7.,La/5.,La/3.,La/1.5,La])
dend = {'La': La, 'D': Da, 'NSEG': 20, 'name':'dend'}

from theory import params_for_cable_theory, stat_pot_function_BRT
from nrn_simulations import *

xtot, cables = set_tree_params(EqCylinder, dend, soma, params)

print cables

fe, fi = 2.,10.

"""
# we calculate the parameters to plug into cable theory
Garray = calculate_mean_conductances(fe, fi, soma, stick, params)
vmean_th = stat_pot_function(x, Garray, soma, stick, params)
"""

# density of synapses in um2, one synapses per this area !
# so calculate synapse number

dt, tstop = 0.025, 10000

exc_synapses, exc_netcons, exc_netstims,\
       inh_synapses, inh_netcons, inh_netstims,\
       area_lists, spkout =\
          Constructing_the_ball_and_tree(params, cables,
                                spiking_mech=False)

for i in range(len(cables)):
    for j in range(len(area_lists[i])):
        # excitation
        Ke = cables[i]['Ke_per_seg']
        exc_netstims[i][j].interval = 1e3/fe/Ke
        Ki = cables[i]['Ki_per_seg']
        inh_netstims[i][j].interval = 1e3/fi/Ki

def get_v():
    v = []
    
    v.append(np.array([[nrn.cable_0_0(.5)._ref_v[0]]])) # somatic potential
    
    # now dendritic potentials
    for level in range(1, len(cables)):
        v0 = []
        for comp in range(max(1,2**(level-1))):
            v1 = []
            exec('section = nrn.cable_'+str(level)+'_'+str(comp))
            for seg in section:
                exec('v1.append(nrn.cable_'+str(level)+'_'+str(comp)+'('+\
                     str(seg.x)+')._ref_v[0])')
            v0.append(np.array(v1))
        v.append(np.array(v0))
        
    return v

## --- recording
t_vec = nrn.Vector()
t_vec.record(nrn._ref_t)
## --- launching the simulation
nrn.finitialize(params['El']*1e3)
nrn.dt = dt
V = []

while nrn.t<(tstop-dt):
    V.append(get_v())
    nrn.fadvance()

V = np.array(V)
t = np.array(t_vec)[:-1]

vmean, vstd = np.zeros(len(xtot)), np.zeros(len(xtot))
ix = 0
for j in range(len(cables)):
    dix = cables[j]['NSEG']
    vmean[ix:dix+ix] = np.array([V[it][j].mean(axis=0) for it in range(len(t))]).mean(axis=0)
    vstd[ix:dix+ix] = np.array([V[it][j] for it in range(len(t))]).std(axis=(0,1))
    ix +=dix


np.save('data/fe'+str(int(fe))+'_fi'+str(int(fi))+'.npy',\
     [fe, fi, xtot, vmean, vstd, params, cables, soma, EqCylinder, dend])

             
# print '======================================'
# print '-------------------------------------------'
# print '---------CONTINUOUS APPROX -------------------'
# print '-------------------------------------------'


# xtot = np.linspace(0,La)
# muV = stat_pot_function_BRT(xtot, fe, fi, cables, params)
# plt.plot(1e6*xtot, 1e3*muV, 'r', lw=3)

# """
# dt, tstop = 1e-5, 50e-3
# t = np.arange(int(tstop/dt))*dt
# f = rfft.time_to_freq(len(t), dt)
# sv2 = np.zeros(len(x))
# for ix_dest in range(len(x)):

#     TF = np.zeros(len(f))
#     # excitatory synapse at dendrites
#     Gf2 = exp_FT_mod(f, params['Qe'], params['Te'])
#     TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
#                 f, Gf2, fe, params['Ee'], 1./stick['exc_density'],\
#                 Garray, soma, stick, params)
                
#     # inhibitory synapse at dendrites
#     Gf2 = exp_FT_mod(f, params['Qi'], params['Ti'])
#     TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
#                 f, Gf2, fi, params['Ei'], 1./stick['inh_density'],\
#                 Garray, soma, stick, params)

#     # inhibitory synapse at soma
#     TF0 = dv_kernel_per_I(x[ix_dest], 0.,\
#                     f, Garray, stick, soma, params)
#     TF2 = TF0*(params['Ei']-muV[0])
#     TF2 *= exp_FT(f, params['Qi'], params['Ti'])
#     TF += 2*fi*soma['Ki_per_seg']*np.abs(TF2)**2


#     # ##then Fourier transform !
#     sv2[ix_dest] = np.trapz(TF, f)
# std_th = np.sqrt(sv2)


# print '----------------------------------------------------'
# print ' end calculus'
# print '----------------------------------------------------'


# plt.fill_between(1e6*x, 1e3*(std_th+muV), 1e3*(muV-std_th),\
#                  color='r', alpha=.3)
# plt.plot(1e6*x, 1e3*muV, 'r', lw=3, label='analytic approx.')
# plt.legend(loc='best', prop={'size':'small'})
# plt.xlabel('distance from soma ($\mu$m)')
# plt.ylabel('membrane pot. (mV)')
# plt.tight_layout()
# plt.show()

# # plt.plot(t, V[:,0], 'b-')
# # plt.plot(t, V[:,-1], 'r-')
# """

# plt.show()
