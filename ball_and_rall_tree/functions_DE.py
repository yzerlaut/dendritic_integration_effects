# functions_DE.py
from neuron import h as nrn
import numpy as np
#nrn.nrn_load_dll("/home/yann/files/tools/neuron/nrn_models/x86_64/.libs/libnrnmech.so.0")

def theory_with_Dendrites(f_exc, f_inh, params, cable):
    """
    args : f_exc, f_inh, params, cable
    params : membrane and synaptic parameters
    cable : cable parameters, array of elements [diam, L, nseg]
    """

    soma = cable[0]
    soma_area = soma['L']*soma['diam']*np.pi*1e-12 # m2

    if len(cable)==1:
        print "---> single compartment"
        ge_mean = f_exc*params['Ne']*params['Qe']*params['Te']*1e-6 #uS
        gi_mean = f_inh*params['Ni']*params['Qi']*params['Ti']*1e-6 
        gl = params['g_pas']*soma_area*1e10 # uS
        Vsoma = params['El']*gl + params['Ei']*gi_mean + params['Ee']*ge_mean
        Vsoma /= (gl + ge_mean + gi_mean)

    else:
        print "---> extended neuron"
        dend_area = params['area_tot']*1e-12-soma_area
        # cable parameters
        D0 = cable[1]['diam'] # trunc diameter
        rm = 1/np.pi/D0/params['g_pas']*1e2 # 1/um * cm2/S -> 1e2 O.m
        ri = params['ra']/(np.pi*D0**2/4)*1e10 # O.cm * 1/mu2 -> 1e10 O/m
        lbd0 = np.sqrt(rm/ri)
        L = lbd0*np.array([cable[i+1]['L']/2**(i/3.)
                           for i in range(len(cable)-1)]).sum()

        # total mean dendritic conductance
        ge_dend_mean = params['Ne']*f_exc*params['Qe']*params['Te']*1e-12
        gi_dend_mean = params['Ni']*(1-params['r_soma_inh'])*\
          f_inh*params['Qi']*params['Ti']*1e-15 # --> Hz.pS.ms -> 1e-15 S
        # total mean somatic conductance
        gi_soma_mean = params['Ni']*params['r_soma_inh']*\
          f_inh*params['Qi']*params['Ti']*1e-15
        # total leak somatic conductance
        gl_soma_mean = params['g_pas']*soma_area*1e4 # S/cm2.m2 -> 1e4 S
        # synaptic densities on dendrites
        dend_g_e = ge_dend_mean/dend_area
        dend_g_i = gi_dend_mean/dend_area

        # conductance gamma factors
        gamma_e = dend_g_e*rm*np.pi*D0
        gamma_i = dend_g_i*rm*np.pi*D0
        K = 1+gamma_e+gamma_i

        # dendritic rescaled potential
        V0dend = 1e-3*(params['El']+gamma_e*params['Ee']+gamma_i*params['Ei'])/K
        # somatic rescaled potential
        V0soma = 1e-3*(gl_soma_mean*params['El']+gi_soma_mean*params['Ei'])\
                           /(gl_soma_mean+gi_soma_mean)

        # junction current
        I_junc = V0soma - V0dend
        I_junc/= 1/(np.sqrt(rm*ri)*K*np.tanh(K*L))-1/(gl_soma_mean+gi_soma_mean)

        Vsoma = 1e3*(V0soma + I_junc/(gl_soma_mean+gi_soma_mean))
    return Vsoma


def HHsimulation_with_Dendrites(f_exc, f_inh, params, cable,
                                dt = 0.1, tstop=10000,\
                                spiking_mech=True):
    """
    args : f_exc, f_inh, params, cable, dt, tstop, spiking_mech
    params : membrane and synaptic parameters
    cable : cable parameters, array of elements [diam, L, nseg]

    to construct the morphology, we need to construct the section in
    NEURON because easier to delete them and weird behavior of python sections
    
    we run a simulation with excitatory and inhibitory synapses
    of an extended neuron (soma + dendrites)
    the inhibition is spread netween soma and dendrites
    the excitation is uniformely spread on the dendrites with a constant
    surfacic density
    """

    ## ---- SPREADING SYNAPSES
    factor_per_sec_per_seg = np.zeros(len(cable))
    # we need the fraction of synapses per segment, per section
    #    ---> related to segments surface
    for ff in range(len(factor_per_sec_per_seg)):
        # actually we compute here only the surface
        factor_per_sec_per_seg[ff] = \
          cable[ff]['L']*cable[ff]['diam']*np.pi / cable[ff]['NSEG']

    # then we get the number of synapses according to the relative surfaces
    Nexc_per_sec_per_seg = factor_per_sec_per_seg
    Nexc_per_sec_per_seg *= params['Ne']/factor_per_sec_per_seg.sum()
    Ninh_per_sec_per_seg = factor_per_sec_per_seg/factor_per_sec_per_seg.sum()
    Ninh_per_sec_per_seg *= params['Ni']*(1-params['r_soma_inh'])
    # params['r_soma_inh'] : % ratio of somatic synapses
    if len(cable)<2: # single compartment
        Nexc_per_sec_per_seg[0] = params['Ne']
        Ninh_per_sec_per_seg[0] = params['Ni']
    else: # no excitatory synapses at soma
        Nexc_per_sec_per_seg[0] = 1e-9
        Ninh_per_sec_per_seg[0] = params['Ni']*params['r_soma_inh']
    exc_synapse, exc_netcon, exc_netstim = [], [], [] # list of excitatory stuff
    inh_synapse, inh_netcon, inh_netstim = [], [], [] # list of inhibitory stuff

    ## --- CREATING THE NEURON
    area_tot = 0
    for level in range(len(cable)):
        for comp in range(max(1,2**(level-1))):
                nrn('create cable_'+str(int(level))+'_'+\
                    str(int(comp)))
                exec('section = nrn.cable_'+str(level)+'_'+str(comp))

                ## --- geometric properties
                section.L = cable[level]['L']
                section.diam = cable[level]['diam']
                section.nseg = cable[level]['NSEG']
                
                ## --- passive properties
                section.Ra = params['ra']
                section.cm = params['cm']
                section.insert('pas')
                section.g_pas = params['g_pas']
                section.e_pas = params['El']

                if level>1: # if not soma
                    # in NEURON : connect daughter, mother branch
                    nrn('connect cable_'+str(int(level))+'_'+\
                    str(int(comp))+'(0), '+\
                    'cable_'+str(int(level-1))+\
                    '_'+str(int(comp/2.))+'(1)')
                ## --- SPREADING THE SYNAPSES (bis)
                for seg in section:
                    # in each segment, we insert an excitatory synapse
                    netstim = nrn.NetStim(seg.x, sec=section)
                    netstim.noise, netstim.number, netstim.start = \
                              1, 1e20, 0
                    netstim.interval = \
                              1e3/f_exc/Nexc_per_sec_per_seg[level-1]
                    exc_netstim.append(netstim)
                    syn = nrn.ExpSyn(seg.x, sec=section)
                    syn.tau, syn.e = params['Te'], params['Ee']
                    exc_synapse.append(syn)
                    netcon = nrn.NetCon(netstim, syn)
                    netcon.weight[0] = params['Qe']/1e3
                    exc_netcon.append(netcon)
                    # then in each segment, we insert an inhibitory synapse
                    netstim = nrn.NetStim(seg.x, sec=section)
                    netstim.noise, netstim.number, netstim.start = \
                          1, 1e20, 0
                    netstim.interval = \
                             1e3/f_inh/Ninh_per_sec_per_seg[level-1]
                    inh_netstim.append(netstim)
                    syn = nrn.ExpSyn(seg.x, sec=section)
                    syn.tau, syn.e = params['Ti'], params['Ei']
                    inh_synapse.append(syn)
                    netcon = nrn.NetCon(netstim, syn)
                    netcon.weight[0] = params['Qi']/1e3
                    inh_netcon.append(netcon)

                    # then just the area to check
                    area_tot+=nrn.area(seg.x, sec=section)


    print "======================================="
    print " --- checking if the neuron is created "
    nrn.topology()
    print "with the total surface : ", area_tot
    print "======================================="

    ## --- spiking properties
    if spiking_mech: # only at soma if we want it !
        nrn('cable_0_0 insert hh_hippoc')
        # + spikes recording !
        counter = nrn.APCount(0.5, sec=nrn.cable_0_0)
        spkout = nrn.Vector()
        counter.thresh = -30
        counter.record(spkout)

    ## --- recording
    v_vec, t_vec = nrn.Vector(), nrn.Vector()
    v_vec.record(nrn.cable_0_0(0.5)._ref_v)
    t_vec.record(nrn._ref_t)
    ## --- launching the simulation
    nrn.finitialize(params['El'])
    nrn.dt = dt
    while nrn.t<tstop:
        nrn.fadvance()
    if spiking_mech:
        fout = spkout.size()/nrn.t*1e3
    else:
        fout = 0
    print "output frequency", fout
    print "======================================="
    nrn('forall delete_section()')
    print " --- checking if the neuron is detroyed"
    nrn.topology()
    print "======================================="
    return np.array(v_vec), np.array(t_vec), fout
