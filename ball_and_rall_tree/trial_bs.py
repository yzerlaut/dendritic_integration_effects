from neuron import h as nrn
import numpy as np
# nrn.nrn_load_dll("/home/yann/files/tools/neuron/nrn_models/x86_64/.libs/libnrnmech.so.0")
import matplotlib.pylab as plt

from functions_DE import *

# ------- model parameters ------- # 
params = { 'area_tot' : 20000, 
'g_pas': 5e-5, 'cm' : 1, 'ra' : 35.4, 'El': -70.,\
'Ne' : 8000. , 'Qe' : .4 , 'Te' : 5., 'Ee': 0.,\
'Ni' : 2000. , 'Qi' : 0.8 , 'Ti' : 5., 'Ei': -80.,\
'seed' : 0, 'r_soma_inh' : 0.8}


# building the different neuronal models
def building_models(params, B_max = 4, nseg0=100, L_s=600):
    cables = []
    # single compartment -> cables[0]
    d = np.sqrt(params['area_tot']/np.pi)
    soma_sc = {'L': d, 'diam': d, 'NSEG': 1}
    cables.append([soma_sc])
    # now extended models (they all have the same soma)
    soma = {'L': 20, 'diam': 20, 'NSEG': 1}
    area_soma = soma['L']*soma['diam']*np.pi
    # compartments of length L_s/Branching_index
    for model in range(1,B_max):
        cable = []
        cable.append(soma) # one soma
        L_comp = L_s/model
        area_left = params['area_tot']-area_soma
        D0 = area_left/np.pi/L_comp \
              /np.array([2**(k/3.) for k in range(model)]).sum()
        # we build one model for each
        for i in range(model):
            cable.append({'L':L_comp, 'diam': D0/2**(2*i/3.), \
                           'NSEG': nseg0})
        cables.append(cable)
    return cables


cables = building_models(params, B_max=6)
print cables

f_exc, f_inh = 2, 4
vmean =  theory_with_Dendrites(f_exc, f_inh, params, cables[2])
# v, t, fout = HHsimulation_with_Dendrites(f_exc, f_inh, params,\
#                                          cables[2], \
#                                          dt = 0.01, tstop=1000,\
#                                          spiking_mech=False)
v, t, fout = run_simulations(f_exc, f_inh, params,\
                                         cables[2], \
                                         dt = 0.01, tstop=1000,\
                                         spiking_mech=False)

plt.plot(t/1e3,v)
plt.plot(t/1e3,vmean*np.ones(t.size), lw=4, alpha=.5)
plt.show()
