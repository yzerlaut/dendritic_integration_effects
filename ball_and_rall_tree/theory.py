import numpy as np
import matplotlib
import matplotlib.pylab as plt

def params_for_cable_theory(cable, Params):
    """
    applies the radial symmetry, to get the linear cable theory parameters
    """
    D = cable['D']
    cable['rm'] = 1./Params['g_pas']/(np.pi*D) # [O.m]   """" NEURON 1e-1 !!!! """" 
    cable['ri'] = Params['Ra']/(np.pi*D**2/4) # [O/m]
    cable['cm'] = Params['cm']*np.pi*D # [F/m] """" NEURON 1e-2 !!!! """"

def set_tree_params(EqCylinder, dend, soma, Params):
    """ returns the different diameters of the equivalent cylinder
    given a number of branches point"""
    cables, xtot = [], np.zeros(1)
    cables.append(soma.copy())
    cables[0]['inh_density'] = Params['inh_density_soma']
    cables[0]['exc_density'] = Params['exc_density_soma']
    Ke_tot, Ki_tot = 0, 0
    D = dend['D'] # mothers branch diameter
    for i in range(1,len(EqCylinder)):
        cable = dend.copy()
        cable['x1'], cable['x2'] = EqCylinder[i-1], EqCylinder[i]
        cable['L'] = cable['x2']-cable['x1']
        dx = cable['L']/cable['NSEG']
        cable['x'] = EqCylinder[i-1]+dx/2+np.arange(cable['NSEG'])*dx
        xtot = np.concatenate([xtot, cable['x']])
        cable['D'] = D*2**(-2*(i-1)/3.)
        cable['inh_density'] = Params['inh_density_dend']
        cable['exc_density'] = Params['exc_density_dend']
        cables.append(cable)

    Ke_tot, Ki_tot, jj = 0, 0, 0
    for cable in cables:
        cable['Ki_per_seg'] = cable['L']*\
          cable['D']*np.pi/cable['NSEG']/cable['inh_density']
        cable['Ke_per_seg'] = cable['L']*\
          cable['D']*np.pi/cable['NSEG']/cable['exc_density']
        # summing over duplicate of compartments
        Ki_tot += 2**jj*cable['Ki_per_seg']*cable['NSEG']
        Ke_tot += 2**jj*cable['Ke_per_seg']*cable['NSEG']
        if cable['name']!='soma':
            jj+=1
    print "Total number of EXCITATORY synapses : ", Ke_tot
    print "Total number of INHIBITORY synapses : ", Ki_tot
    return xtot, cables

def parameters_for_mean_BRT(fe, fi, cables, Params):

    soma = cables[0]
    [Ls, Ds] = soma['L'], soma['D']
    Gl = np.pi*Ls*Ds*Params['g_pas']
    Cm = np.pi*Ls*Ds*Params['cm']
    [El, Ei, Ee] = Params['El'], Params['Ei'], Params['Ee']
    
    # radial density of excitatory conductance
    Te, Qe = Params['Te'], Params['Qe']
    ge0_rm = Qe*fe*Te/Params['exc_density_dend']/Params['g_pas']
    
    # radial density of inhibitory conductance
    Ti, Qi = Params['Ti'], Params['Qi']
    gi0_rm = Qi*fi*Ti/Params['inh_density_dend']/Params['g_pas']
    
    # radial density of inhibitory conductance at soma
    Gi0_Rm = Qi*fi*Ti/Params['inh_density_soma']/Params['g_pas']
    
    # potentials
    v0 = (El+ge0_rm*Ee+gi0_rm*Ei)/(1+ge0_rm+gi0_rm)
    V0 = (El+Gi0_Rm*Ei)/(1+Gi0_Rm)

    # time constants (doesn't depends on the diameter)
    Tau = Params['cm']/Params['g_pas']/(1+Gi0_Rm)
    tau = Params['cm']/Params['g_pas']/(1+ge0_rm+gi0_rm)

    # now the different lambda, as a power of the first lbd=lbd0
    LBD, LL, LRmax = [], [], 0
    D = cables[1]['D']
    lbd0 = np.sqrt(D/4/Params['g_pas']/Params['Ra']/(1+ge0_rm+gi0_rm))
    for i in range(1, len(cables)): # discard soma
        lbd = lbd0*2**(-(i-1)/3.)
        LBD.append(lbd)
        LL.append(cables[i]['L'])
        LRmax += cables[i]['L']/lbd
    return [tau, Tau, np.array(LBD), np.array(LL), LRmax, v0, V0]


def rescale_x_BRT(x, LL, LBD, return_branch_number=False):
    Lsum = np.cumsum(np.concatenate([[0],LL]))
    i, X = 1, 0
    while (x>Lsum[i]) :
        X+=LL[i-1]/LBD[i-1]
        i+=1
    return X+(x-Lsum[i-1])/LBD[min([i-1, len(LBD)-1])] # to be corrected

def get_branch_number(x, LL, LBD): # returns the branch where x lies
    Lsum = np.cumsum(np.concatenate([[0],LL]))
    i = 1
    while (x>Lsum[i]) :
        i+=1
    return i

def stat_pot_function_BRT(x, fe, fi, cables, Params):
    
    soma = cables[0]
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    
    tau, Tau, LBD, LL, LRmax, v0, V0 = \
      parameters_for_mean_BRT(fe, fi, cables, Params)

    ri = Params['Ra']/(np.pi*cables[1]['D']**2/4) # [O/m]
    denom = 1/np.tanh(LRmax)+(Tau)/(ri*Cm*LBD[0])
    X = np.array([rescale_x_BRT(xx, LL, LBD) for xx in x])
    return v0+(V0-v0)/denom*np.cosh(X-LRmax)/np.sinh(LRmax)


def dv_kernel_per_I_BRT(x, X, f, fe, fi, dend, soma, cables, Params):
    cm, ri = cables[1]['cm'], cables[1]['ri']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    
    tau, Tau, LBD, LL, LRmax, v0, V0 = \
      parameters_for_mean_BRT(fe, fi, cables, Params)

    xR, XR = [rescale_x_BRT(xx, LL, LBD) for xx in [x, X]] # rescaling
    lbd = LBD[0] # input lbd0
    Af = 1./(1+2*1j*np.pi*f*tau) # unitary input for the kernel

    Fdep = np.sqrt(1+2*1j*np.pi*f*tau)
    lbdF = lbd/Fdep # with frequency dependency
    Bf = lbdF*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau
    factor = Af*tau/cm/lbdF/( np.sinh(LRmax*Fdep) + Bf*np.cosh(LRmax*Fdep) )
    if xR<XR:
        func = (np.cosh(xR*Fdep) + Bf*np.sinh(xR*Fdep))*np.cosh((XR-LRmax)*Fdep)
    else:
        func = (np.cosh(XR*Fdep) + Bf*np.sinh(XR*Fdep))*np.cosh((xR-LRmax)*Fdep)
    return func*factor

def get_the_transfer_resistance(fe, fi, x, params, cables, soma, stick):
    Rtf = np.zeros(len(x))
    tau, Tau, LBD, LL, LRmax, v0, V0 = \
      parameters_for_mean_BRT(fe, fi, cables, params)
    
    for ix_dest in range(len(x)):
        BB = get_branch_number(x[ix_dest], LL, LBD)
        Rtf[ix_dest] = dv_kernel_per_I_BRT(0., x[ix_dest],\
                            0., fe, fi, stick, soma, cables, params)/BB
    return Rtf

def expr0(U, B, af, bf):      
    return np.abs( np.sinh(U*(af+1j*bf)) + B*np.cosh(U*(af+1j*bf)) )**2

def expr1(U, af, bf):
    # return np.abs(np.cosh(U*(af+1j*bf)))**2
    return .5*(np.cosh(2*af*U)+np.cos(2*bf*U))
    
def expr2(U, B, af, bf):      
    return np.abs( np.cosh(U*(af+1j*bf)) + B*np.sinh(U*(af+1j*bf)) )**2

def exp_FT(f, Q, Tsyn, t0=0):
    return Q*np.exp(-1j*2*np.pi*t0*f)/(1j*2*np.pi*f+1./Tsyn)

def exp_FT_mod(f, Q, Tsyn):
    return Q**2/((2*np.pi*f)**2+(1./Tsyn)**2)


def split_root_square_of_imaginary(f, tau):
    # returns the ral and imaginary part of Sqrt(1+2*Pi*f*tau)
    af = np.sqrt((np.sqrt(1+(2*np.pi*f*tau)**2)+1)/2)
    bf = np.sqrt((np.sqrt(1+(2*np.pi*f*tau)**2)-1)/2)
    return af, bf

def dv2_at_soma_contrib_per_dend_synapse_type_BRT(f, fe, fi, Gf2,\
                            syn_freq, Erev, Density,\
                            soma, cables, Params,
                            precision=1e1):

    cm, ri, D = cables[1]['cm'], cables[1]['ri'], cables[1]['D']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    
    tau, Tau, LBD, LL, LRmax, v0, V0 = \
      parameters_for_mean_BRT(fe, fi, cables, Params)

    lbd = LBD[0] # input lbd0
    v1 = (V0-v0)/( 1/np.tanh(LRmax)+(Tau)/(ri*Cm*lbd) )/np.sinh(LRmax)
    Af2 = 1./(1+(2*np.pi*f*tau)**2) # unitary input for the kernel

    af, bf = split_root_square_of_imaginary(f, tau)
    
    Fdep = np.sqrt(1+2*1j*np.pi*f*tau)
    lbdF = lbd/Fdep # with frequency dependency
    Bf = lbdF*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau



    LL = np.concatenate([[0],LL])
    output = np.zeros(len(f))
    for i in range(len(f)):
        factor = Af2[i]*tau**2/cm**2/lbd**2*(af[i]**2+bf[i]**2)/\
          expr0(LRmax, Bf[i], af[i], bf[i])

        for j in range(len(LBD)):
            lbd1, x1, x2 = LBD[j], LL[j], LL[j+1]
            XR2 = np.linspace(rescale_x_BRT(x1, LL, LBD),\
                              rescale_x_BRT(x2, LL, LBD), precision)
            Y2 = (Erev-v0-v1*np.cosh(XR2-LRmax))**2\
                  *expr1(XR2-LRmax, af[i], bf[i])
            Y2 /= (j+1) # branch number = j+1
            output[i] += factor*(\
                    expr2(0., Bf[i], af[i], bf[i])*np.trapz(Y2, XR2))*lbd1

    return 2*syn_freq*np.pi*D*Density*output*Gf2

def get_the_theoretical_variance(fe, fi, f, params, soma, dend, cables, muV0):

    TF = np.zeros(len(f))
    # excitatory synapse at dendrites
    Gf2 = exp_FT_mod(f, params['Qe'], params['Te'])
    TF += dv2_at_soma_contrib_per_dend_synapse_type_BRT(f, fe, fi, Gf2,\
                            fe, params['Ee'], 1./params['exc_density_dend'],\
                            soma, cables, params)

    # inhibitory synapse at dendrites
    Gf2 = exp_FT_mod(f, params['Qi'], params['Ti'])
    TF += dv2_at_soma_contrib_per_dend_synapse_type_BRT(f, fe, fi, Gf2,\
                            fi, params['Ei'], 1./params['inh_density_dend'],\
                            soma, cables, params)


    # inhibitory synapse at soma
    soma['Ki_per_seg'] = soma['L']*\
          soma['D']*np.pi/soma['NSEG']/params['inh_density_soma']
    TF0 = dv_kernel_per_I_BRT(0., 0., f, fe, fi, dend, soma, cables, params)
    TF2 = TF0*(params['Ei']-muV0)
    TF2 *= exp_FT(f, params['Qi'], params['Ti'])
    TF += 2*fi*soma['Ki_per_seg']*np.abs(TF2)**2

    return 2*np.trapz(TF, f)

def dv2_contrib_per_dend_synapse_type(x, f, Gf2,\
                            syn_freq, Erev, Density, Garray,\
                            soma, stick, Params,
                            precision=1e2):
    [ri, L, D, cm] = stick['ri'], stick['L'], stick['D'], stick['cm']
    [Ls, Ds] = soma['L'], soma['D']
    Cm = np.pi*Ls*Ds*Params['cm']
    ge0, gi0, Gi0 = Garray # mean conductance input
    [tau, Tau, lbd, v0, V0] = parameters_for_mean(ge0, gi0, Gi0,\
                                    soma, stick, Params)
    v1 = (V0-v0)/( 1/np.tanh(L/lbd)+(Tau)/(ri*Cm*lbd) )/np.sinh(L/lbd)
    
    Af2 = 1./(1+(2*np.pi*f*tau)**2) # unitary input for the kernel
    af, bf = split_root_square_of_imaginary(f, tau)
    Bf = lbd*Cm*ri*(1+2*1j*np.pi*f*Tau)/Tau/(af+1j*bf)

    X1 = np.linspace(0, x, precision)
    X2 = np.linspace(x, L, precision)
    
    output = np.zeros(len(f))
    for i in range(len(f)):
        factor = Af2[i]*tau**2/cm**2/lbd**2*(af[i]**2+bf[i]**2)/\
          expr0(L/lbd, Bf[i], af[i], bf[i])

        Y1 = (Erev-v0-v1*np.cosh((X1-L)/lbd))**2\
                  *expr2(X1/lbd, Bf[i], af[i], bf[i])
        Y2 = (Erev-v0-v1*np.cosh((X2-L)/lbd))**2\
                  *expr1((X2-L)/lbd, af[i], bf[i])
        output[i] = factor*(\
            expr1((x-L)/lbd, af[i], bf[i])*np.trapz(Y1, X1)+\
            expr2(x/lbd, Bf[i], af[i], bf[i])*np.trapz(Y2, X2))


    return 2*syn_freq*np.pi*D*Density*output*Gf2
                  

def get_the_mean_weighted_transfer_resistance(fe, fi, x, params,\
                        cables, soma, stick):
    Rtf, Weight = np.zeros(len(x)), np.zeros(len(x))
    tau, Tau, LBD, LL, LRmax, v0, V0 = \
      parameters_for_mean_BRT(fe, fi, cables, params)
    
    for ix_dest in range(len(x)):
        
        BB = get_branch_number(x[ix_dest], LL, LBD)
        cable = cables[BB]
        cable['Ki_per_seg'] = cable['L']*\
          cable['D']*np.pi/cable['NSEG']/cable['inh_density']
        cable['Ke_per_seg'] = cable['L']*\
          cable['D']*np.pi/cable['NSEG']/cable['exc_density']
        
        Rtf[ix_dest] = dv_kernel_per_I_BRT(0., x[ix_dest],\
                            0., fe, fi, stick, soma, cables, params)/BB
                            
        Weight[ix_dest] = BB*(cable['Ki_per_seg']+cable['Ke_per_seg'])

    Weight[0] += soma['Ki_per_seg'] # + somatic synapses at 0
    
    return np.mean(Rtf*Weight/Weight.sum())


