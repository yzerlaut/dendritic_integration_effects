import numpy as np
import matplotlib.pylab as plt

import sys
sys.path.append('/home/yann/work/yns_python_library')
from theory import *
import fourier_for_real as rfft


fe, fi, xtot, vmean, vstd, params, cables, soma, EqCylinder, dend = \
  np.load(sys.argv[-1])
xth = np.linspace(0,dend['La'], 1e3)

for cable in cables:
    params_for_cable_theory(cable, params)

    
# membrane potential
muV = stat_pot_function_BRT(xth, fe, fi, cables, params)
dt, tstop = 1e-5, 50e-3
t = np.arange(int(tstop/dt))*dt
f = rfft.time_to_freq(len(t), dt)
sV = np.sqrt(get_the_theoretical_variance(fe, fi, f, params, soma, dend, cables, muV[0]))

plt.figure()
plt.errorbar(1e6*xtot, vmean, yerr=vstd,color='b', label='num. sim.')
plt.plot(1e6*xth, 1e3*muV, 'r', lw=1, label='analytic approx.')
plt.errorbar([0], [1e3*muV[0]], yerr=[1e3*sV], color='r', lw=5)
plt.legend(loc='best', prop={'size':'small'})
plt.ylabel('$V_m$ (mV)')

plt.figure()
Rtf = get_the_transfer_resistance(fe, fi, xth, params, cables, soma, dend)
plt.plot(1e6*xth, 1e-6*Rtf, 'r', label='analytic approx.')
plt.ylabel('$R_{TF}$ (M$\Omega$)')

plt.xlabel('distance from soma ($\mu$m)')

# """
# dt, tstop = 1e-5, 50e-3
# t = np.arange(int(tstop/dt))*dt
# f = rfft.time_to_freq(len(t), dt)
# sv2 = np.zeros(len(x))
# for ix_dest in range(len(x)):

#     TF = np.zeros(len(f))
#     # excitatory synapse at dendrites
#     Gf2 = exp_FT_mod(f, params['Qe'], params['Te'])
#     TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
#                 f, Gf2, fe, params['Ee'], 1./stick['exc_density'],\
#                 Garray, soma, stick, params)
                
#     # inhibitory synapse at dendrites
#     Gf2 = exp_FT_mod(f, params['Qi'], params['Ti'])
#     TF += dv2_contrib_per_dend_synapse_type(x[ix_dest],\
#                 f, Gf2, fi, params['Ei'], 1./stick['inh_density'],\
#                 Garray, soma, stick, params)

#     # inhibitory synapse at soma
#     TF0 = dv_kernel_per_I(x[ix_dest], 0.,\
#                     f, Garray, stick, soma, params)
#     TF2 = TF0*(params['Ei']-muV[0])
#     TF2 *= exp_FT(f, params['Qi'], params['Ti'])
#     TF += 2*fi*soma['Ki_per_seg']*np.abs(TF2)**2


#     # ##then Fourier transform !
#     sv2[ix_dest] = np.trapz(TF, f)
# std_th = np.sqrt(sv2)


# print '----------------------------------------------------'
# print ' end calculus'
# print '----------------------------------------------------'


# plt.fill_between(1e6*x, 1e3*(std_th+muV), 1e3*(muV-std_th),\
#                  color='r', alpha=.3)
# plt.plot(1e6*x, 1e3*muV, 'r', lw=3, label='analytic approx.')
# plt.show()

# # plt.plot(t, V[:,0], 'b-')
# # plt.plot(t, V[:,-1], 'r-')
# """

plt.tight_layout()
plt.show()
