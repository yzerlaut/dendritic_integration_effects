import numpy as np
import matplotlib.pylab as plt

from matplotlib.patches import Circle, Rectangle
from matplotlib.lines import Line2D
from matplotlib.collections import PatchCollection

# GLOBAL VARIABLES !!!
Rm = 1e-1
Ri = 35.4*1e-2
Cm = 1e-2
DISCRET0 = 50

DDY = 5 # shift from the soma for initiating the dendrites

# --- Soma
DS = 20*1e-6
lS = 20*1e-6

la = 500
Da = 7
EqCylinderA = np.array([0.,la/12.,la/7.,la/5.,la/1.5,la/1.2,la])

def vec_diameter(D,EqCylinder):
    """ returns the different diameters of the equivalent cylinder
    given a number of branches point"""
    DVEC = []
    DVEC.append(D)
    for i in range(1,len(EqCylinder)-1):
        D0 = D*2**(-2*i/3)
        DVEC.append(D0)
    return DVEC

vecDA = vec_diameter(Da,EqCylinderA)

def patches_generator(vecD,EqCylinder,DX, DY=0.1, xsoma=0.25, ysoma=0.25):
    """
    generates the patches to plot the different dendritic trees
    vecD = array with the different diameters
    EqCylinder = array of the branching points
    DX = array of the spacing between branches
    """
    links, patches = [], []
    Pos = []
    Pos2 = [0]
    # we start by the soma
    patches.append(Circle((0,0), radius=xsoma))
    # then the small line between soma and 
    links.append(Line2D((0,0),(ysoma,ysoma+DY)) )
    # then initial branch
    y = ysoma+DY
    diameter = vecD[0]
    length = EqCylinder[1]
    patches.append(Rectangle((-diameter/2,y),diameter,length)) # first branch apical
    for i in range(1,len(vecDA)):
        Pos = Pos2
        Pos2 = []
        diameter = vecD[i]
        y = y + DY + length
        length = EqCylinder[i+1]-EqCylinder[i]
        for j in range(len(Pos)):
            x1 = Pos[j]-DX[i]/2.-diameter
            x2 = Pos[j]+DX[i]/2.
            patches.append(Rectangle((x1,y),diameter,length))
            patches.append(Rectangle((x2,y),diameter,length))
            links.append(Line2D((x1,x2),(y,y)))
            links.append(Line2D((Pos[j],Pos[j]),(y-DY,y)))
            Pos2.append(x1+diameter/2)
            Pos2.append(x2+diameter/2)
    return links, patches



## --- Apical Dendrite
# Xa = np.zeros(1) # the zero will have to be deleted
# for i in range(EqCylinderA.size-1):
#     Xa = np.concatenate([Xa,Xa[-1]+np.linspace(0,(EqCylinderA[i+1]-EqCylinderA[i])/lbdA[i],DISCRET0)])
# Xa = Xa[1:]


fig = plt.figure(figsize=(9,3))
patches = []

ax = plt.subplot(161,frame_on=False)
# ax.set_title('B=0')
# ax.set_xticks([])
# ax.set_yticks([])
# c0 = Circle((0,1),radius=.25, lw=2, facecolor='w')
# ax.add_artist(c0)
# plt.axis('equal')
# plt.xlim([-1,1])
# # plt.annotate('single \n compartment', (0,-.5), ha='center')

plt.title('B=1')
ax.set_xticks([]);ax.set_yticks([])

DXA = [0]
EqCylinderA = [0,1.5]
vecDA = vec_diameter(.2,EqCylinderA)

links, patches = patches_generator(vecDA, EqCylinderA, DXA, xsoma=.2,ysoma=.2)

p0 = PatchCollection(patches, linewidth=2, facecolor='w');ax.add_collection(p0)
p0 = PatchCollection(links, linewidth=.5);ax.add_collection(p0)
plt.axis('equal')
plt.ylim([-1.7,2.8])
# ax.annotate('ball \& stick', (0,-1), ha='center')

ax = plt.subplot(162,frame_on=False)
plt.title('B=2')
ax.set_xticks([]);ax.set_yticks([])

DXA = [0,.3]
EqCylinderA = [0,1,2]
vecDA = vec_diameter(.2,EqCylinderA)

links, patches = patches_generator(vecDA, EqCylinderA, DXA, xsoma=.15,ysoma=.15)

p0 = PatchCollection(patches, linewidth=2, facecolor='w');ax.add_collection(p0)
p0 = PatchCollection(links, linewidth=.5);ax.add_collection(p0)
plt.axis('equal')
plt.ylim([-1,3])

ax = plt.subplot(163,frame_on=False)
plt.title('B=3')
ax.set_xticks([]);ax.set_yticks([])
DXA = [0,1,.5]
EqCylinderA = [0, 1, 2, 3]
vecDA = vec_diameter(.2,EqCylinderA)

links, patches = patches_generator(vecDA, EqCylinderA, DXA, xsoma=.15,ysoma=.15)

p0 = PatchCollection(patches, linewidth=2, facecolor='w');ax.add_collection(p0)
p0 = PatchCollection(links, linewidth=.5);ax.add_collection(p0)
plt.axis('equal')
plt.ylim([0,3])


ax = plt.subplot(164,frame_on=False)
plt.title('B=4')
ax.set_xticks([]);ax.set_yticks([])

DXA = [0,3, 1, .5 , .25, .1, .01, .01]
EqCylinderA = [0, 1, 2, 3, 4]
vecDA = vec_diameter(.2,EqCylinderA)

links, patches = patches_generator(vecDA, EqCylinderA, DXA, xsoma=.15,ysoma=.15)

p0 = PatchCollection(patches, linewidth=2, facecolor='w');ax.add_collection(p0)
p0 = PatchCollection(links, linewidth=.5);ax.add_collection(p0)
plt.axis('equal')
#plt.ylim([-.2,2])

ax = plt.subplot(165,frame_on=False)
plt.title('B=5')
ax.set_xticks([]);ax.set_yticks([])

DXA = [0,3, 1, .5 , .3, .1, .01, .01, .01]
EqCylinderA = [0, 1, 2, 3, 4, 5]
vecDA = vec_diameter(.2,EqCylinderA)

links, patches = patches_generator(vecDA, EqCylinderA, DXA, xsoma=.15,ysoma=.15)

p0 = PatchCollection(patches, linewidth=2, facecolor='w');ax.add_collection(p0)
p0 = PatchCollection(links, linewidth=.5);ax.add_collection(p0)
plt.axis('equal')


ax = plt.subplot(166,frame_on=False)
plt.title('B=6')
ax.set_xticks([]);ax.set_yticks([])

DXA = [0,3, 1, .5 , .3, .1, .01, .01, .01, .01]
EqCylinderA = [0, 1, 2, 3, 4, 5, 6]
vecDA = vec_diameter(.2,EqCylinderA)

links, patches = patches_generator(vecDA, EqCylinderA, DXA, xsoma=.15,ysoma=.15)

p0 = PatchCollection(patches, linewidth=2, facecolor='w');ax.add_collection(p0)
p0 = PatchCollection(links, linewidth=.5);ax.add_collection(p0)
plt.axis('equal')


plt.tight_layout()
#fig.savefig('paper/f0.pdf')
plt.show()


